from ConfigurablePlayer import ConfigurablePlayer


class Cluster6(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.5752

    def pre_regular_call_called_before(self):
        return 0.647

    def pre_regular_call_raised_before(self):
        return 0.7337

    def pre_regular_raise_nobody_before(self):
        return 0.1006

    def pre_regular_raise_called_before(self):
        return 0.0812

    def pre_regular_raise_raised_before(self):
        return 0.0714

    def pre_button_call_nobody_before(self):
        return 0.2511

    def pre_button_call_called_before(self):
        return 0.6322

    def pre_button_call_raised_before(self):
        return 0.6434

    def pre_button_raise_nobody_before(self):
        return 0.2573

    def pre_button_raise_called_before(self):
        return 0.114

    def pre_button_raise_raised_before(self):
        return 0.1144

    def post_regular_call_called_before(self):
        return 0.6454

    def post_regular_call_raised_before(self):
        return 0.6026

    def post_regular_raise_nobody_before(self):
        return 0.2779

    def post_regular_raise_called_before(self):
        return 0.0948

    def post_regular_raise_raised_before(self):
        return 0.1058

    def post_button_call_called_before(self):
        return 0.6086

    def post_button_call_raised_before(self):
        return 0.5705

    def post_button_raise_nobody_before(self):
        return 0.5016

    def post_button_raise_called_before(self):
        return 0.1608

    def post_button_raise_raised_before(self):
        return 0.1749
