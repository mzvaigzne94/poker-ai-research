from ConfigurablePlayer import ConfigurablePlayer


class Cluster2(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.3094

    def pre_regular_call_called_before(self):
        return 0.4238

    def pre_regular_call_raised_before(self):
        return 0.5037

    def pre_regular_raise_nobody_before(self):
        return 0.1865

    def pre_regular_raise_called_before(self):
        return 0.1651

    def pre_regular_raise_raised_before(self):
        return 0.1521

    def pre_button_call_nobody_before(self):
        return 0.2511

    def pre_button_call_called_before(self):
        return 0.4213

    def pre_button_call_raised_before(self):
        return 0.4646

    def pre_button_raise_nobody_before(self):
        return 0.2573

    def pre_button_raise_called_before(self):
        return 0.2009

    def pre_button_raise_raised_before(self):
        return 0.1774

    def post_regular_call_called_before(self):
        return 0.5072

    def post_regular_call_raised_before(self):
        return 0.4702

    def post_regular_raise_nobody_before(self):
        return 0.4184

    def post_regular_raise_called_before(self):
        return 0.1949

    def post_regular_raise_raised_before(self):
        return 0.2062

    def post_button_call_called_before(self):
        return 0.4619

    def post_button_call_raised_before(self):
        return 0.4116

    def post_button_raise_nobody_before(self):
        return 0.6147

    def post_button_raise_called_before(self):
        return 0.3017

    def post_button_raise_raised_before(self):
        return 0.3452
