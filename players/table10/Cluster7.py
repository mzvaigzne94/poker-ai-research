from ConfigurablePlayer import ConfigurablePlayer


class Cluster7(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.2495

    def pre_regular_call_called_before(self):
        return 0.3054

    def pre_regular_call_raised_before(self):
        return 0.3811

    def pre_regular_raise_nobody_before(self):
        return 0.3876

    def pre_regular_raise_called_before(self):
        return 0.5295

    def pre_regular_raise_raised_before(self):
        return 0.4928

    def pre_button_call_nobody_before(self):
        return 0.2511

    def pre_button_call_called_before(self):
        return 0.4127

    def pre_button_call_raised_before(self):
        return 0.4638

    def pre_button_raise_nobody_before(self):
        return 0.2573

    def pre_button_raise_called_before(self):
        return 0.3241

    def pre_button_raise_raised_before(self):
        return 0.2611

    def post_regular_call_called_before(self):
        return 0.4242

    def post_regular_call_raised_before(self):
        return 0.3671

    def post_regular_raise_nobody_before(self):
        return 0.6457

    def post_regular_raise_called_before(self):
        return 0.4025

    def post_regular_raise_raised_before(self):
        return 0.4332

    def post_button_call_called_before(self):
        return 0.4189

    def post_button_call_raised_before(self):
        return 0.3217

    def post_button_raise_nobody_before(self):
        return 0.6542

    def post_button_raise_called_before(self):
        return 0.4181

    def post_button_raise_raised_before(self):
        return 0.5206
