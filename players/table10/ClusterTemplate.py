from ConfigurablePlayer import ConfigurablePlayer


class Cluster(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.

    def pre_regular_call_called_before(self):
        return 0.

    def pre_regular_call_raised_before(self):
        return 0.

    def pre_regular_raise_nobody_before(self):
        return 0.

    def pre_regular_raise_called_before(self):
        return 0.

    def pre_regular_raise_raised_before(self):
        return 0.

    def pre_button_call_nobody_before(self):
        return 0.

    def pre_button_call_called_before(self):
        return 0.

    def pre_button_call_raised_before(self):
        return 0.

    def pre_button_raise_nobody_before(self):
        return 0.

    def pre_button_raise_called_before(self):
        return 0.

    def pre_button_raise_raised_before(self):
        return 0.

    def post_regular_call_called_before(self):
        return 0.

    def post_regular_call_raised_before(self):
        return 0.

    def post_regular_raise_nobody_before(self):
        return 0.

    def post_regular_raise_called_before(self):
        return 0.

    def post_regular_raise_raised_before(self):
        return 0.

    def post_button_call_called_before(self):
        return 0.

    def post_button_call_raised_before(self):
        return 0.

    def post_button_raise_nobody_before(self):
        return 0.

    def post_button_raise_called_before(self):
        return 0.

    def post_button_raise_raised_before(self):
        return 0.
