from ConfigurablePlayer import ConfigurablePlayer


class Cluster4(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.2853

    def pre_regular_call_called_before(self):
        return 0.3927

    def pre_regular_call_raised_before(self):
        return 0.4715

    def pre_regular_raise_nobody_before(self):
        return 0.0716

    def pre_regular_raise_called_before(self):
        return 0.0567

    def pre_regular_raise_raised_before(self):
        return 0.0463

    def pre_button_call_nobody_before(self):
        return 0.2511

    def pre_button_call_called_before(self):
        return 0.3971

    def pre_button_call_raised_before(self):
        return 0.4244

    def pre_button_raise_nobody_before(self):
        return 0.2573

    def pre_button_raise_called_before(self):
        return 0.0909

    def pre_button_raise_raised_before(self):
        return 0.0877

    def post_regular_call_called_before(self):
        return 0.5655

    def post_regular_call_raised_before(self):
        return 0.5289

    def post_regular_raise_nobody_before(self):
        return 0.3083

    def post_regular_raise_called_before(self):
        return 0.087

    def post_regular_raise_raised_before(self):
        return 0.1002

    def post_button_call_called_before(self):
        return 0.5658

    def post_button_call_raised_before(self):
        return 0.5428

    def post_button_raise_nobody_before(self):
        return 0.5586

    def post_button_raise_called_before(self):
        return 0.1558

    def post_button_raise_raised_before(self):
        return 0.1694
