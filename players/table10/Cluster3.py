from ConfigurablePlayer import ConfigurablePlayer


class Cluster3(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.1503

    def pre_regular_call_called_before(self):
        return 0.255

    def pre_regular_call_raised_before(self):
        return 0.2984

    def pre_regular_raise_nobody_before(self):
        return 0.0819

    def pre_regular_raise_called_before(self):
        return 0.06

    def pre_regular_raise_raised_before(self):
        return 0.0479

    def pre_button_call_nobody_before(self):
        return 0.2509

    def pre_button_call_called_before(self):
        return 0.2532

    def pre_button_call_raised_before(self):
        return 0.2618

    def pre_button_raise_nobody_before(self):
        return 0.2573

    def pre_button_raise_called_before(self):
        return 0.0898

    def pre_button_raise_raised_before(self):
        return 0.0818

    def post_regular_call_called_before(self):
        return 0.4659

    def post_regular_call_raised_before(self):
        return 0.4388

    def post_regular_raise_nobody_before(self):
        return 0.3494

    def post_regular_raise_called_before(self):
        return 0.1101

    def post_regular_raise_raised_before(self):
        return 0.1249

    def post_button_call_called_before(self):
        return 0.4969

    def post_button_call_raised_before(self):
        return 0.4906

    def post_button_raise_nobody_before(self):
        return 0.6041

    def post_button_raise_called_before(self):
        return 0.1957

    def post_button_raise_raised_before(self):
        return 0.2072
