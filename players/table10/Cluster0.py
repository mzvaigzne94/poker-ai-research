from ConfigurablePlayer import ConfigurablePlayer


class Cluster0(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.3158

    def pre_regular_call_called_before(self):
        return 0.419

    def pre_regular_call_raised_before(self):
        return 0.4947

    def pre_regular_raise_nobody_before(self):
        return 0.1826

    def pre_regular_raise_called_before(self):
        return 0.2249

    def pre_regular_raise_raised_before(self):
        return 0.2037

    def pre_button_call_nobody_before(self):
        return 0.2511

    def pre_button_call_called_before(self):
        return 0.4364

    def pre_button_call_raised_before(self):
        return 0.471

    def pre_button_raise_nobody_before(self):
        return 0.2573

    def pre_button_raise_called_before(self):
        return 0.1754

    def pre_button_raise_raised_before(self):
        return 0.1558

    def post_regular_call_called_before(self):
        return 0.5219

    def post_regular_call_raised_before(self):
        return 0.4585

    def post_regular_raise_nobody_before(self):
        return 0.4682

    def post_regular_raise_called_before(self):
        return 0.2397

    def post_regular_raise_raised_before(self):
        return 0.2879

    def post_button_call_called_before(self):
        return 0.4993

    def post_button_call_raised_before(self):
        return 0.4753

    def post_button_raise_nobody_before(self):
        return 0.599

    def post_button_raise_called_before(self):
        return 0.2616

    def post_button_raise_raised_before(self):
        return 0.2927
