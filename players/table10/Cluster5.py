from ConfigurablePlayer import ConfigurablePlayer


class Cluster5(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.3243

    def pre_regular_call_called_before(self):
        return 0.3827

    def pre_regular_call_raised_before(self):
        return 0.4994

    def pre_regular_raise_nobody_before(self):
        return 0.172

    def pre_regular_raise_called_before(self):
        return 0.1718

    def pre_regular_raise_raised_before(self):
        return 0.1643

    def pre_button_call_nobody_before(self):
        return 0.251

    def pre_button_call_called_before(self):
        return 0.4365

    def pre_button_call_raised_before(self):
        return 0.471

    def pre_button_raise_nobody_before(self):
        return 0.2572

    def pre_button_raise_called_before(self):
        return 0.1753

    def pre_button_raise_raised_before(self):
        return 0.1558

    def post_regular_call_called_before(self):
        return 0.5324

    def post_regular_call_raised_before(self):
        return 0.5314

    def post_regular_raise_nobody_before(self):
        return 0.4003

    def post_regular_raise_called_before(self):
        return 0.196

    def post_regular_raise_raised_before(self):
        return 0.2046

    def post_button_call_called_before(self):
        return 0.4991

    def post_button_call_raised_before(self):
        return 0.4753

    def post_button_raise_nobody_before(self):
        return 0.5955

    def post_button_raise_called_before(self):
        return 0.2617

    def post_button_raise_raised_before(self):
        return 0.2923
