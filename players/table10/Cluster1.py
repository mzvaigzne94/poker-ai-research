from ConfigurablePlayer import ConfigurablePlayer


class Cluster1(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.5135

    def pre_regular_call_called_before(self):
        return 0.6373

    def pre_regular_call_raised_before(self):
        return 0.7057

    def pre_regular_raise_nobody_before(self):
        return 0.2142

    def pre_regular_raise_called_before(self):
        return 0.1668

    def pre_regular_raise_raised_before(self):
        return 0.1572

    def pre_button_call_nobody_before(self):
        return 0.2516

    def pre_button_call_called_before(self):
        return 0.4855

    def pre_button_call_raised_before(self):
        return 0.5102

    def pre_button_raise_nobody_before(self):
        return 0.257

    def pre_button_raise_called_before(self):
        return 0.2341

    def pre_button_raise_raised_before(self):
        return 0.1877

    def post_regular_call_called_before(self):
        return 0.6577

    def post_regular_call_raised_before(self):
        return 0.6134

    def post_regular_raise_nobody_before(self):
        return 0.3413

    def post_regular_raise_called_before(self):
        return 0.1469

    def post_regular_raise_raised_before(self):
        return 0.152

    def post_button_call_called_before(self):
        return 0.606

    def post_button_call_raised_before(self):
        return 0.6324

    def post_button_raise_nobody_before(self):
        return 0.5792

    def post_button_raise_called_before(self):
        return 0.2226

    def post_button_raise_raised_before(self):
        return 0.1949
