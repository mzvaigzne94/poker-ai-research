from ConfigurablePlayer import ConfigurablePlayer


class Cluster0(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.4694

    def pre_regular_call_called_before(self):
        return 0.5573

    def pre_regular_call_raised_before(self):
        return 0.6481

    def pre_regular_raise_nobody_before(self):
        return 0.1667

    def pre_regular_raise_called_before(self):
        return 0.1302

    def pre_regular_raise_raised_before(self):
        return 0.1202

    def pre_button_call_nobody_before(self):
        return 0.2889

    def pre_button_call_called_before(self):
        return 0.5186

    def pre_button_call_raised_before(self):
        return 0.5459

    def pre_button_raise_nobody_before(self):
        return 0.2786

    def pre_button_raise_called_before(self):
        return 0.1821

    def pre_button_raise_raised_before(self):
        return 0.1758

    def post_regular_call_called_before(self):
        return 0.5834

    def post_regular_call_raised_before(self):
        return 0.5492

    def post_regular_raise_nobody_before(self):
        return 0.3439

    def post_regular_raise_called_before(self):
        return 0.1433

    def post_regular_raise_raised_before(self):
        return 0.1408

    def post_button_call_called_before(self):
        return 0.5254

    def post_button_call_raised_before(self):
        return 0.5101

    def post_button_raise_nobody_before(self):
        return 0.553

    def post_button_raise_called_before(self):
        return 0.2494

    def post_button_raise_raised_before(self):
        return 0.2393
