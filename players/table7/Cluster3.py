from ConfigurablePlayer import ConfigurablePlayer


class Cluster3(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.3277

    def pre_regular_call_called_before(self):
        return 0.4294

    def pre_regular_call_raised_before(self):
        return 0.5186

    def pre_regular_raise_nobody_before(self):
        return 0.2296

    def pre_regular_raise_called_before(self):
        return 0.2202

    def pre_regular_raise_raised_before(self):
        return 0.207

    def pre_button_call_nobody_before(self):
        return 0.2881

    def pre_button_call_called_before(self):
        return 0.4419

    def pre_button_call_raised_before(self):
        return 0.4951

    def pre_button_raise_nobody_before(self):
        return 0.2797

    def pre_button_raise_called_before(self):
        return 0.2249

    def pre_button_raise_raised_before(self):
        return 0.2055

    def post_regular_call_called_before(self):
        return 0.5233

    def post_regular_call_raised_before(self):
        return 0.4754

    def post_regular_raise_nobody_before(self):
        return 0.4975

    def post_regular_raise_called_before(self):
        return 0.2374

    def post_regular_raise_raised_before(self):
        return 0.2806

    def post_button_call_called_before(self):
        return 0.4872

    def post_button_call_raised_before(self):
        return 0.4471

    def post_button_raise_nobody_before(self):
        return 0.5938

    def post_button_raise_called_before(self):
        return 0.3035

    def post_button_raise_raised_before(self):
        return 0.3483
