from ConfigurablePlayer import ConfigurablePlayer


class Cluster1(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.2715

    def pre_regular_call_called_before(self):
        return 0.3429

    def pre_regular_call_raised_before(self):
        return 0.4224

    def pre_regular_raise_nobody_before(self):
        return 0.3219

    def pre_regular_raise_called_before(self):
        return 0.3749

    def pre_regular_raise_raised_before(self):
        return 0.3441

    def pre_button_call_nobody_before(self):
        return 0.2881

    def pre_button_call_called_before(self):
        return 0.404

    def pre_button_call_raised_before(self):
        return 0.4677

    def pre_button_raise_nobody_before(self):
        return 0.2797

    def pre_button_raise_called_before(self):
        return 0.2945

    def pre_button_raise_raised_before(self):
        return 0.253

    def post_regular_call_called_before(self):
        return 0.4638

    def post_regular_call_raised_before(self):
        return 0.415

    def post_regular_raise_nobody_before(self):
        return 0.5421

    def post_regular_raise_called_before(self):
        return 0.3084

    def post_regular_raise_raised_before(self):
        return 0.3264

    def post_button_call_called_before(self):
        return 0.442

    def post_button_call_raised_before(self):
        return 0.3818

    def post_button_raise_nobody_before(self):
        return 0.634

    def post_button_raise_called_before(self):
        return 0.3675

    def post_button_raise_raised_before(self):
        return 0.42
