from ConfigurablePlayer import ConfigurablePlayer


class Cluster2(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.2932

    def pre_regular_call_called_before(self):
        return 0.3769

    def pre_regular_call_raised_before(self):
        return 0.4667

    def pre_regular_raise_nobody_before(self):
        return 0.202

    def pre_regular_raise_called_before(self):
        return 0.1789

    def pre_regular_raise_raised_before(self):
        return 0.1773

    def pre_button_call_nobody_before(self):
        return 0.2877

    def pre_button_call_called_before(self):
        return 0.4179

    def pre_button_call_raised_before(self):
        return 0.4801

    def pre_button_raise_nobody_before(self):
        return 0.2803

    def pre_button_raise_called_before(self):
        return 0.2159

    def pre_button_raise_raised_before(self):
        return 0.1998

    def post_regular_call_called_before(self):
        return 0.5221

    def post_regular_call_raised_before(self):
        return 0.5027

    def post_regular_raise_nobody_before(self):
        return 0.3414

    def post_regular_raise_called_before(self):
        return 0.2087

    def post_regular_raise_raised_before(self):
        return 0.1943

    def post_button_call_called_before(self):
        return 0.487

    def post_button_call_raised_before(self):
        return 0.4717

    def post_button_raise_nobody_before(self):
        return 0.5834

    def post_button_raise_called_before(self):
        return 0.2974

    def post_button_raise_raised_before(self):
        return 0.298
