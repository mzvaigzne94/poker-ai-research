from ConfigurablePlayer import ConfigurablePlayer


class Cluster2(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.2903

    def pre_regular_call_called_before(self):
        return 0.3773

    def pre_regular_call_raised_before(self):
        return 0.4662

    def pre_regular_raise_nobody_before(self):
        return 0.3822

    def pre_regular_raise_called_before(self):
        return 0.3243

    def pre_regular_raise_raised_before(self):
        return 0.3092

    def pre_button_call_nobody_before(self):
        return 0.2788

    def pre_button_call_called_before(self):
        return 0.3966

    def pre_button_call_raised_before(self):
        return 0.492

    def pre_button_raise_nobody_before(self):
        return 0.3634

    def pre_button_raise_called_before(self):
        return 0.3065

    def pre_button_raise_raised_before(self):
        return 0.2887

    def post_regular_call_called_before(self):
        return 0.5044

    def post_regular_call_raised_before(self):
        return 0.4558

    def post_regular_raise_nobody_before(self):
        return 0.5218

    def post_regular_raise_called_before(self):
        return 0.268

    def post_regular_raise_raised_before(self):
        return 0.287

    def post_button_call_called_before(self):
        return 0.4835

    def post_button_call_raised_before(self):
        return 0.4083

    def post_button_raise_nobody_before(self):
        return 0.616

    def post_button_raise_called_before(self):
        return 0.3154

    def post_button_raise_raised_before(self):
        return 0.3823
