from ConfigurablePlayer import ConfigurablePlayer


class Cluster0(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.3672

    def pre_regular_call_called_before(self):
        return 0.4525

    def pre_regular_call_raised_before(self):
        return 0.5458

    def pre_regular_raise_nobody_before(self):
        return 0.234

    def pre_regular_raise_called_before(self):
        return 0.1474

    def pre_regular_raise_raised_before(self):
        return 0.134

    def pre_button_call_nobody_before(self):
        return 0.3202

    def pre_button_call_called_before(self):
        return 0.4493

    def pre_button_call_raised_before(self):
        return 0.5507

    def pre_button_raise_nobody_before(self):
        return 0.2824

    def pre_button_raise_called_before(self):
        return 0.1993

    def pre_button_raise_raised_before(self):
        return 0.1783

    def post_regular_call_called_before(self):
        return 0.5293

    def post_regular_call_raised_before(self):
        return 0.5069

    def post_regular_raise_nobody_before(self):
        return 0.3746

    def post_regular_raise_called_before(self):
        return 0.1801

    def post_regular_raise_raised_before(self):
        return 0.1501

    def post_button_call_called_before(self):
        return 0.4937

    def post_button_call_raised_before(self):
        return 0.4866

    def post_button_raise_nobody_before(self):
        return 0.5609

    def post_button_raise_called_before(self):
        return 0.2825

    def post_button_raise_raised_before(self):
        return 0.2477
