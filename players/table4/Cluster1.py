from ConfigurablePlayer import ConfigurablePlayer


class Cluster1(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.331

    def pre_regular_call_called_before(self):
        return 0.4125

    def pre_regular_call_raised_before(self):
        return 0.5105

    def pre_regular_raise_nobody_before(self):
        return 0.2975

    def pre_regular_raise_called_before(self):
        return 0.2297

    def pre_regular_raise_raised_before(self):
        return 0.213

    def pre_button_call_nobody_before(self):
        return 0.3014

    def pre_button_call_called_before(self):
        return 0.4253

    def pre_button_call_raised_before(self):
        return 0.5241

    def pre_button_raise_nobody_before(self):
        return 0.3192

    def pre_button_raise_called_before(self):
        return 0.2477

    def pre_button_raise_raised_before(self):
        return 0.2284

    def post_regular_call_called_before(self):
        return 0.518

    def post_regular_call_raised_before(self):
        return 0.4936

    def post_regular_raise_nobody_before(self):
        return 0.4419

    def post_regular_raise_called_before(self):
        return 0.22

    def post_regular_raise_raised_before(self):
        return 0.2499

    def post_button_call_called_before(self):
        return 0.4891

    def post_button_call_raised_before(self):
        return 0.4669

    def post_button_raise_nobody_before(self):
        return 0.5595

    def post_button_raise_called_before(self):
        return 0.2975

    def post_button_raise_raised_before(self):
        return 0.3295
