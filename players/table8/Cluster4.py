from ConfigurablePlayer import ConfigurablePlayer


class Cluster4(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.1952

    def pre_regular_call_called_before(self):
        return 0.3071

    def pre_regular_call_raised_before(self):
        return 0.3634

    def pre_regular_raise_nobody_before(self):
        return 0.1206

    def pre_regular_raise_called_before(self):
        return 0.0907

    def pre_regular_raise_raised_before(self):
        return 0.0856

    def pre_button_call_nobody_before(self):
        return 0.2533

    def pre_button_call_called_before(self):
        return 0.3123

    def pre_button_call_raised_before(self):
        return 0.3775

    def pre_button_raise_nobody_before(self):
        return 0.2626

    def pre_button_raise_called_before(self):
        return 0.1504

    def pre_button_raise_raised_before(self):
        return 0.1462

    def post_regular_call_called_before(self):
        return 0.4752

    def post_regular_call_raised_before(self):
        return 0.4374

    def post_regular_raise_nobody_before(self):
        return 0.3778

    def post_regular_raise_called_before(self):
        return 0.1556

    def post_regular_raise_raised_before(self):
        return 0.1547

    def post_button_call_called_before(self):
        return 0.4787

    def post_button_call_raised_before(self):
        return 0.4528

    def post_button_raise_nobody_before(self):
        return 0.6035

    def post_button_raise_called_before(self):
        return 0.2653

    def post_button_raise_raised_before(self):
        return 0.2617
