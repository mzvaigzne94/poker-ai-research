from ConfigurablePlayer import ConfigurablePlayer


class Cluster2(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.3171

    def pre_regular_call_called_before(self):
        return 0.4232

    def pre_regular_call_raised_before(self):
        return 0.5051

    def pre_regular_raise_nobody_before(self):
        return 0.2192

    def pre_regular_raise_called_before(self):
        return 0.2287

    def pre_regular_raise_raised_before(self):
        return 0.205

    def pre_button_call_nobody_before(self):
        return 0.2533

    def pre_button_call_called_before(self):
        return 0.4405

    def pre_button_call_raised_before(self):
        return 0.4845

    def pre_button_raise_nobody_before(self):
        return 0.2626

    def pre_button_raise_called_before(self):
        return 0.2092

    def pre_button_raise_raised_before(self):
        return 0.1898

    def post_regular_call_called_before(self):
        return 0.5265

    def post_regular_call_raised_before(self):
        return 0.4582

    def post_regular_raise_nobody_before(self):
        return 0.4855

    def post_regular_raise_called_before(self):
        return 0.2496

    def post_regular_raise_raised_before(self):
        return 0.3046

    def post_button_call_called_before(self):
        return 0.4905

    def post_button_call_raised_before(self):
        return 0.4536

    def post_button_raise_nobody_before(self):
        return 0.5946

    def post_button_raise_called_before(self):
        return 0.2954

    def post_button_raise_raised_before(self):
        return 0.3283
