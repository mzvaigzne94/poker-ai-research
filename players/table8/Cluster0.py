from ConfigurablePlayer import ConfigurablePlayer


class Cluster0(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.326

    def pre_regular_call_called_before(self):
        return 0.4018

    def pre_regular_call_raised_before(self):
        return 0.5101

    def pre_regular_raise_nobody_before(self):
        return 0.2103

    def pre_regular_raise_called_before(self):
        return 0.2021

    def pre_regular_raise_raised_before(self):
        return 0.1931

    def pre_button_call_nobody_before(self):
        return 0.2533

    def pre_button_call_called_before(self):
        return 0.4406

    def pre_button_call_raised_before(self):
        return 0.4845

    def pre_button_raise_nobody_before(self):
        return 0.2626

    def pre_button_raise_called_before(self):
        return 0.2095

    def pre_button_raise_raised_before(self):
        return 0.1898

    def post_regular_call_called_before(self):
        return 0.5258

    def post_regular_call_raised_before(self):
        return 0.5092

    def post_regular_raise_nobody_before(self):
        return 0.4146

    def post_regular_raise_called_before(self):
        return 0.2218

    def post_regular_raise_raised_before(self):
        return 0.2308

    def post_button_call_called_before(self):
        return 0.491

    def post_button_call_raised_before(self):
        return 0.4598

    def post_button_raise_nobody_before(self):
        return 0.5873

    def post_button_raise_called_before(self):
        return 0.2959

    def post_button_raise_raised_before(self):
        return 0.3361
