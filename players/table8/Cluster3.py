from ConfigurablePlayer import ConfigurablePlayer


class Cluster3(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.4552

    def pre_regular_call_called_before(self):
        return 0.5528

    def pre_regular_call_raised_before(self):
        return 0.6454

    def pre_regular_raise_nobody_before(self):
        return 0.1658

    def pre_regular_raise_called_before(self):
        return 0.1337

    def pre_regular_raise_raised_before(self):
        return 0.1237

    def pre_button_call_nobody_before(self):
        return 0.2533

    def pre_button_call_called_before(self):
        return 0.5246

    def pre_button_call_raised_before(self):
        return 0.5472

    def pre_button_raise_nobody_before(self):
        return 0.2626

    def pre_button_raise_called_before(self):
        return 0.1855

    def pre_button_raise_raised_before(self):
        return 0.1736

    def post_regular_call_called_before(self):
        return 0.5851

    def post_regular_call_raised_before(self):
        return 0.5448

    def post_regular_raise_nobody_before(self):
        return 0.3457

    def post_regular_raise_called_before(self):
        return 0.1441

    def post_regular_raise_raised_before(self):
        return 0.1467

    def post_button_call_called_before(self):
        return 0.5268

    def post_button_call_raised_before(self):
        return 0.5044

    def post_button_raise_nobody_before(self):
        return 0.552

    def post_button_raise_called_before(self):
        return 0.254

    def post_button_raise_raised_before(self):
        return 0.2442
