from ConfigurablePlayer import ConfigurablePlayer


class Cluster1(ConfigurablePlayer):
    def pre_regular_call_nobody_before(self):
        return 0.2789

    def pre_regular_call_called_before(self):
        return 0.3584

    def pre_regular_call_raised_before(self):
        return 0.4442

    def pre_regular_raise_nobody_before(self):
        return 0.3301

    def pre_regular_raise_called_before(self):
        return 0.3803

    def pre_regular_raise_raised_before(self):
        return 0.3513

    def pre_button_call_nobody_before(self):
        return 0.2533

    def pre_button_call_called_before(self):
        return 0.4165

    def pre_button_call_raised_before(self):
        return 0.4746

    def pre_button_raise_nobody_before(self):
        return 0.2626

    def pre_button_raise_called_before(self):
        return 0.2918

    def pre_button_raise_raised_before(self):
        return 0.2477

    def post_regular_call_called_before(self):
        return 0.4606

    def post_regular_call_raised_before(self):
        return 0.4147

    def post_regular_raise_nobody_before(self):
        return 0.5472

    def post_regular_raise_called_before(self):
        return 0.3146

    def post_regular_raise_raised_before(self):
        return 0.336

    def post_button_call_called_before(self):
        return 0.4428

    def post_button_call_raised_before(self):
        return 0.3645

    def post_button_raise_nobody_before(self):
        return 0.6292

    def post_button_raise_called_before(self):
        return 0.3779

    def post_button_raise_raised_before(self):
        return 0.4454
