from pypokerengine.players import BasePokerPlayer
from calculatedStartingCardRanks import *
from HelperFunctions import index_hand_dictionary, find_percentage_for_hand, find_percentage_for_specific_hand, estimate_hole_card_win_rate, safe_div
from math import dist
from weka.core.dataset import Instance
from weka.classifiers import Classifier


# This is the extended base class that should be extended to create AI player that plays according to given
# probabilities in defined situations and also outputs statistics from the game about how well different models
# have predicted players class

class ConfigurablePlayer(BasePokerPlayer):
    SUITS = ['C', 'D', 'H', 'S']
    PAIRED_SUITS = [['C', 'D'], ['C', 'H'], ['C', 'S'], ['D', 'H'], ['D', 'S'], ['H', 'S']]
    NOT_PAIRED_SUITS = [['C', 'D'], ['C', 'H'], ['C', 'S'], ['D', 'H'], ['D', 'S'], ['H', 'S'], ['D', 'C'], ['H', 'C'], ['S', 'C'], ['H', 'D'], ['S', 'D'], ['S', 'H']]

    def __init__(self):
        super().__init__()
        self.total_players = 4
        self.number_of_players = 4
        self.ranked_hands = STARTING_4.copy()
        self.rank_one = ''
        self.rank_two = ''
        self.suit_one = ''
        self.suit_two = ''
        self.name = 'test.txt'
        self.is_suited = False
        self.is_all_hands_generated = False
        self.distances = []
        self.multilayer_classifier, self.multilayer_data = Classifier.deserialize("H:\\Universitate\\meta_data\\multilayer-4.model")
        self.ada_classifier, self.ada_data = Classifier.deserialize("H:\\Universitate\\meta_data\\adaboostm1-lmt-4.model")
        self.bayes_net_classifier, self.bayes_net_data = Classifier.deserialize("H:\\Universitate\\meta_data\\bayes_net-4.model")
        self.ada_history = []
        self.multilayer_history = []
        self.bayes_net_history = []
        self.centers_10 = [
            [0.3158, 0.419, 0.4947, 0.1826, 0.2249, 0.2037, 0.2511, 0.4364, 0.471, 0.2573, 0.1754, 0.1558, 0.5219, 0.4585, 0.4682, 0.2397, 0.2879, 0.4993, 0.4753, 0.599, 0.2616, 0.2927],
            [0.5135, 0.6373, 0.7057, 0.2142, 0.1668, 0.1572, 0.2516, 0.4855, 0.5102, 0.257, 0.2341, 0.1877, 0.6577, 0.6134, 0.3413, 0.1469, 0.152, 0.606, 0.6324, 0.5792, 0.2226, 0.1949],
            [0.3094, 0.4238, 0.5037, 0.1865, 0.1651, 0.1521, 0.2511, 0.4213, 0.4646, 0.2573, 0.2009, 0.1774, 0.5072, 0.4702, 0.4184, 0.1949, 0.2062, 0.4619, 0.4116, 0.6147, 0.3017, 0.3452],
            [0.1503, 0.255, 0.2984, 0.0819, 0.06, 0.0479, 0.2509, 0.2532, 0.2618, 0.2573, 0.0898, 0.0818, 0.4659, 0.4388, 0.3494, 0.1101, 0.1249, 0.4969, 0.4906, 0.6041, 0.1957, 0.2072],
            [0.2853, 0.3927, 0.4715, 0.0716, 0.0567, 0.0463, 0.2511, 0.3971, 0.4244, 0.2573, 0.0909, 0.0877, 0.5655, 0.5289, 0.3083, 0.087, 0.1002, 0.5658, 0.5428, 0.5586, 0.1558, 0.1694],
            [0.3243, 0.3827, 0.4994, 0.172, 0.1718, 0.1643, 0.251, 0.4365, 0.471, 0.2572, 0.1753, 0.1558, 0.5324, 0.5314, 0.4003, 0.196, 0.2046, 0.4991, 0.4753, 0.5955, 0.2617, 0.2923],
            [0.5752, 0.647, 0.7337, 0.1006, 0.0812, 0.0714, 0.2511, 0.6322, 0.6434, 0.2573, 0.114, 0.1144, 0.6454, 0.6026, 0.2779, 0.0948, 0.1058, 0.6086, 0.5705, 0.5016, 0.1608, 0.1749],
            [0.2495, 0.3054, 0.3811, 0.3876, 0.5295, 0.4928, 0.2511, 0.4127, 0.4638, 0.2573, 0.3241, 0.2611, 0.4242, 0.3671, 0.6457, 0.4025, 0.4332, 0.4189, 0.3217, 0.6542, 0.4181, 0.5206],
        ]
        self.centers_8 = [
            [0.326, 0.4018, 0.5101, 0.2103, 0.2021, 0.1931, 0.2533, 0.4406, 0.4845, 0.2626, 0.2095, 0.1898, 0.5258, 0.5092, 0.4146, 0.2218, 0.2308, 0.491, 0.4598, 0.5873, 0.2959, 0.3361],
            [0.2789, 0.3584, 0.4442, 0.3301, 0.3803, 0.3513, 0.2533, 0.4165, 0.4746, 0.2626, 0.2918, 0.2477, 0.4606, 0.4147, 0.5472, 0.3146, 0.336, 0.4428, 0.3645, 0.6292, 0.3779, 0.4454],
            [0.3171, 0.4232, 0.5051, 0.2192, 0.2287, 0.205, 0.2533, 0.4405, 0.4845, 0.2626, 0.2092, 0.1898, 0.5265, 0.4582, 0.4855, 0.2496, 0.3046, 0.4905, 0.4536, 0.5946, 0.2954, 0.3283],
            [0.4552, 0.5528, 0.6454, 0.1658, 0.1337, 0.1237, 0.2533, 0.5246, 0.5472, 0.2626, 0.1855, 0.1736, 0.5851, 0.5448, 0.3457, 0.1441, 0.1467, 0.5268, 0.5044, 0.552, 0.254, 0.2442],
            [0.1952, 0.3071, 0.3634, 0.1206, 0.0907, 0.0856, 0.2533, 0.3123, 0.3775, 0.2626, 0.1504, 0.1462, 0.4752, 0.4374, 0.3778, 0.1556, 0.1547, 0.4787, 0.4528, 0.6035, 0.2653, 0.2617],
        ]
        self.centers_7 = [
            [0.4694, 0.5573, 0.6481, 0.1667, 0.1302, 0.1202, 0.2889, 0.5186, 0.5459, 0.2786, 0.1821, 0.1758, 0.5834, 0.5492, 0.3439, 0.1433, 0.1408, 0.5254, 0.5101, 0.553, 0.2494, 0.2393],
            [0.2715, 0.3429, 0.4224, 0.3219, 0.3749, 0.3441, 0.2881, 0.404, 0.4677, 0.2797, 0.2945, 0.253, 0.4638, 0.415, 0.5421, 0.3084, 0.3264, 0.442, 0.3818, 0.634, 0.3675, 0.42],
            [0.2932, 0.3769, 0.4667, 0.202, 0.1789, 0.1773, 0.2877, 0.4179, 0.4801, 0.2803, 0.2159, 0.1998, 0.5221, 0.5027, 0.3414, 0.2087, 0.1943, 0.487, 0.4717, 0.5834, 0.2974, 0.298],
            [0.3277, 0.4294, 0.5186, 0.2296, 0.2202, 0.207, 0.2881, 0.4419, 0.4951, 0.2797, 0.2249, 0.2055, 0.5233, 0.4754, 0.4975, 0.2374, 0.2806, 0.4872, 0.4471, 0.5938, 0.3035, 0.3483],
        ]
        self.centers_4 = [
            [0.3672, 0.4525, 0.5458, 0.234, 0.1474, 0.134, 0.3202, 0.4493, 0.5507, 0.2824, 0.1993, 0.1783, 0.5293, 0.5069, 0.3746, 0.1801, 0.1501, 0.4937, 0.4866, 0.5609, 0.2825, 0.2477],
            [0.331, 0.4125, 0.5105, 0.2975, 0.2297, 0.213, 0.3014, 0.4253, 0.5241, 0.3192, 0.2477, 0.2284, 0.518, 0.4936, 0.4419, 0.22, 0.2499, 0.4891, 0.4669, 0.5595, 0.2975, 0.3295],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        ]
        self.centers = [
            [0.3672, 0.4525, 0.5458, 0.234, 0.1474, 0.134, 0.3202, 0.4493, 0.5507, 0.2824, 0.1993, 0.1783, 0.5293, 0.5069, 0.3746, 0.1801, 0.1501, 0.4937, 0.4866, 0.5609, 0.2825, 0.2477],
            [0.331, 0.4125, 0.5105, 0.2975, 0.2297, 0.213, 0.3014, 0.4253, 0.5241, 0.3192, 0.2477, 0.2284, 0.518, 0.4936, 0.4419, 0.22, 0.2499, 0.4891, 0.4669, 0.5595, 0.2975, 0.3295],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        ]
        self.probabilities = [0.33, 0.33, 0.33]
        self.probabilities_history = []
        self.frequency_data = {
            'pre': {
                'regular': {
                    'call': {
                        'nobody': 0,
                        'nobodyCount': 0,
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    },
                    'raise': {
                        'nobody': 0,
                        'nobodyCount': 0,
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    }
                },
                'button': {
                    'call': {
                        'nobody': 0,
                        'nobodyCount': 0,
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    },
                    'raise': {
                        'nobody': 0,
                        'nobodyCount': 0,
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    }
                }
            },
            'post': {
                'regular': {
                    'call': {
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    },
                    'raise': {
                        'nobody': 0,
                        'nobodyCount': 0,
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    }
                },
                'button': {
                    'call': {
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    },
                    'raise': {
                        'nobody': 0,
                        'nobodyCount': 0,
                        'called': 0,
                        'calledCount': 0,
                        'raised': 0,
                        'raisedCount': 0
                    }
                }
            }
        }
        self.attr_names = {
            'pre': {
                'regular': {
                    'call': [
                        'nobody',
                        'called',
                        'raised'
                    ],
                    'raise': [
                        'nobody',
                        'called',
                        'raised'
                    ]
                },
                'button': {
                    'call': [
                        'nobody',
                        'called',
                        'raised'
                    ],
                    'raise': [
                        'nobody',
                        'called',
                        'raised'
                    ]
                }
            },
            'post': {
                'regular': {
                    'call': [
                        'called',
                        'raised'
                    ],
                    'raise': [
                        'nobody',
                        'called',
                        'raised'
                    ]
                },
                'button': {
                    'call': [
                        'called',
                        'raised'
                    ],
                    'raise': [
                        'nobody',
                        'called',
                        'raised'
                    ]
                }
            }
        }

    def declare_action(self, valid_actions, hole_card, round_state):
        print(valid_actions)
        print(round_state)
        can_raise = False
        can_call = False
        call_amount = 0
        raise_amount = 0
        for action in valid_actions:
            if action['action'] == 'call':
                can_call = True
                call_amount = action['amount']
            if action['action'] == 'raise' and action['amount']['min'] > 0:
                can_raise = True
                raise_amount = action['amount']['min']
        print(round_state['street'])

        if round_state['street'] == 'preflop':
            hand_percentage = find_percentage_for_hand(self.ranked_hands, self.rank_one, self.rank_two, self.is_suited)
        else:
            hand_percentage = find_percentage_for_specific_hand(self.ranked_hands, self.rank_one, self.rank_two, self.suit_one, self.suit_two)

        is_raised = False
        is_called = False
        button_or_last = False
        if round_state['street'] in round_state['action_histories']:
            for action in round_state['action_histories'][round_state['street']]:
                if action['action'] == 'CALL' and action['amount'] > 0:
                    is_called = True
                if action['action'] == 'RAISE':
                    is_raised = True

        dealer_button = round_state['dealer_btn']
        while True:
            if round_state['seats'][dealer_button]['state'] != 'folded':
                if round_state['seats'][dealer_button]['uuid'] == self.uuid:
                    button_or_last = True
                break
            dealer_button -= 1
            if dealer_button < 0:
                dealer_button = self.total_players - 1
        print(hand_percentage, button_or_last, is_called, is_raised, self.ranked_hands)
        is_preflop = round_state['street'] == 'preflop'
        action, amount, percentage, action_index, reverse_indexes = self.get_action_amount_percentage(is_preflop, is_called, is_raised, button_or_last, can_raise, can_call, hand_percentage, call_amount, raise_amount)
        self.update_probabilities(action_index, reverse_indexes)
        if action:
            largest_index = max(self.ranked_hands)
            cutoff_point = round(largest_index * percentage)
            while cutoff_point not in self.ranked_hands:
                cutoff_point += 1
            for key in self.ranked_hands.copy():
                if key > cutoff_point:
                    del self.ranked_hands[key]

            print(action, amount)
            return action, amount

        if can_call and call_amount == 0:
            print('call', 0)
            return 'call', 0
        else:
            print('fold', 0)
            return 'fold', 0

    def update_probabilities(self, action_index, reverse_indexes):
        if action_index is not None:
            new_probabilities = []
            for index, probability in enumerate(self.probabilities):
                new_probabilities.append(probability * self.centers[index][action_index])
        else:
            if len(reverse_indexes) == 2:
                rev_index_1 = reverse_indexes[0]
                rev_index_2 = reverse_indexes[1]
                new_probabilities = []
                for index, probability in enumerate(self.probabilities):
                    new_probabilities.append(probability * (1.0 - self.centers[index][rev_index_1] - self.centers[index][rev_index_2]))
            else:
                rev_index = reverse_indexes[0]
                new_probabilities = []
                for index, probability in enumerate(self.probabilities):
                    new_probabilities.append(probability * (1.0 - self.centers[index][rev_index]))
        total = sum(new_probabilities)
        normalized_probabilities = []
        for probability in new_probabilities:
            normalized_probabilities.append(probability / total)
        self.probabilities_history.append(normalized_probabilities)
        self.probabilities = normalized_probabilities

    def get_action_amount_percentage(self, is_preflop, is_called, is_raised, button_or_last, can_raise, can_call, hand_percentage, call_amount, raise_amount):
        raise_id = None
        call_id = None
        if is_preflop:
            if button_or_last:
                if not is_called and not is_raised:
                    self.frequency_data['pre']['button']['call']['nobodyCount'] += 1
                    self.frequency_data['pre']['button']['raise']['nobodyCount'] += 1
                    if hand_percentage < self.pre_button_raise_nobody_before() and can_raise:
                        self.frequency_data['pre']['button']['raise']['nobody'] += 1
                        return 'raise', raise_amount, self.pre_button_raise_nobody_before(), 9, None
                    if hand_percentage < self.pre_button_raise_nobody_before() + self.pre_button_call_nobody_before() and can_call:
                        self.frequency_data['pre']['button']['call']['nobody'] += 1
                        return 'call', call_amount, self.pre_button_raise_nobody_before() + self.pre_button_call_nobody_before(), 6, None
                    raise_id = 9
                    call_id = 6
                if is_raised:
                    self.frequency_data['pre']['button']['call']['raisedCount'] += 1
                    self.frequency_data['pre']['button']['raise']['raisedCount'] += 1
                    if hand_percentage < self.pre_button_raise_raised_before() and can_raise:
                        if is_called:
                            self.frequency_data['pre']['button']['call']['calledCount'] += 1
                            self.frequency_data['pre']['button']['raise']['calledCount'] += 1
                            self.frequency_data['pre']['button']['raise']['called'] += 1
                        self.frequency_data['pre']['button']['raise']['raised'] += 1
                        return 'raise', raise_amount, self.pre_button_raise_raised_before(), 11, None
                    if hand_percentage < self.pre_button_raise_raised_before() + self.pre_button_call_raised_before() and can_call:
                        if is_called:
                            self.frequency_data['pre']['button']['call']['calledCount'] += 1
                            self.frequency_data['pre']['button']['raise']['calledCount'] += 1
                            self.frequency_data['pre']['button']['call']['called'] += 1
                        self.frequency_data['pre']['button']['call']['raised'] += 1
                        return 'call', call_amount, self.pre_button_raise_raised_before() + self.pre_button_call_raised_before(), 8, None
                    raise_id = 11
                    call_id = 8
                if is_called:
                    self.frequency_data['pre']['button']['call']['calledCount'] += 1
                    self.frequency_data['pre']['button']['raise']['calledCount'] += 1
                    if hand_percentage < self.pre_button_raise_called_before() and can_raise:
                        if is_raised:
                            self.frequency_data['pre']['button']['raise']['raised'] += 1
                        self.frequency_data['pre']['button']['raise']['called'] += 1
                        return 'raise', raise_amount, self.pre_button_raise_called_before(), 10, None
                    if hand_percentage < self.pre_button_raise_called_before() + self.pre_button_call_called_before() and can_call:
                        if is_raised:
                            self.frequency_data['pre']['button']['call']['raised'] += 1
                        self.frequency_data['pre']['button']['call']['called'] += 1
                        return 'call', call_amount, self.pre_button_raise_called_before() + self.pre_button_call_called_before(), 7, None
                    raise_id = 10
                    call_id = 7
            else:
                if not is_called and not is_raised:
                    self.frequency_data['pre']['regular']['call']['nobodyCount'] += 1
                    self.frequency_data['pre']['regular']['raise']['nobodyCount'] += 1
                    if hand_percentage < self.pre_regular_raise_nobody_before() and can_raise:
                        self.frequency_data['pre']['regular']['raise']['nobody'] += 1
                        return 'raise', raise_amount, self.pre_regular_raise_nobody_before(), 3, None
                    if hand_percentage < self.pre_regular_raise_nobody_before() + self.pre_regular_call_nobody_before() and can_call:
                        self.frequency_data['pre']['regular']['call']['nobody'] += 1
                        return 'call', call_amount, self.pre_regular_raise_nobody_before() + self.pre_regular_call_nobody_before(), 0, None
                    raise_id = 3
                    call_id = 0
                if is_raised:
                    self.frequency_data['pre']['regular']['call']['raisedCount'] += 1
                    self.frequency_data['pre']['regular']['raise']['raisedCount'] += 1
                    if hand_percentage < self.pre_regular_raise_raised_before() and can_raise:
                        if is_called:
                            self.frequency_data['pre']['regular']['call']['calledCount'] += 1
                            self.frequency_data['pre']['regular']['raise']['calledCount'] += 1
                            self.frequency_data['pre']['regular']['raise']['called'] += 1
                        self.frequency_data['pre']['regular']['raise']['raised'] += 1
                        return 'raise', raise_amount, self.pre_regular_raise_raised_before(), 5, None
                    if hand_percentage < self.pre_regular_raise_raised_before() + self.pre_regular_call_raised_before() and can_call:
                        if is_called:
                            self.frequency_data['pre']['regular']['call']['calledCount'] += 1
                            self.frequency_data['pre']['regular']['raise']['calledCount'] += 1
                            self.frequency_data['pre']['regular']['call']['called'] += 1
                        self.frequency_data['pre']['regular']['call']['raised'] += 1
                        return 'call', call_amount, self.pre_regular_raise_raised_before() + self.pre_regular_call_raised_before(), 2, None
                    raise_id = 5
                    call_id = 2
                if is_called:
                    self.frequency_data['pre']['regular']['call']['calledCount'] += 1
                    self.frequency_data['pre']['regular']['raise']['calledCount'] += 1
                    if hand_percentage < self.pre_regular_raise_called_before() and can_raise:
                        if is_raised:
                            self.frequency_data['pre']['regular']['raise']['raised'] += 1
                        self.frequency_data['pre']['regular']['raise']['called'] += 1
                        return 'raise', raise_amount, self.pre_regular_raise_called_before(), 4, None
                    if hand_percentage < self.pre_regular_raise_called_before() + self.pre_regular_call_called_before() and can_call:
                        if is_raised:
                            self.frequency_data['pre']['regular']['call']['raised'] += 1
                        self.frequency_data['pre']['regular']['call']['called'] += 1
                        return 'call', call_amount, self.pre_regular_raise_called_before() + self.pre_regular_call_called_before(), 1, None
                    raise_id = 4
                    call_id = 1
        else:
            if button_or_last:
                if not is_called and not is_raised:
                    self.frequency_data['post']['button']['raise']['nobodyCount'] += 1
                    if hand_percentage < self.post_button_raise_nobody_before() and can_raise:
                        self.frequency_data['post']['button']['raise']['nobody'] += 1
                        return 'raise', raise_amount, self.post_button_raise_nobody_before(), 19, None
                    raise_id = 19
                if is_raised:
                    self.frequency_data['post']['button']['call']['raisedCount'] += 1
                    self.frequency_data['post']['button']['raise']['raisedCount'] += 1
                    if hand_percentage < self.post_button_raise_raised_before() and can_raise:
                        if is_called:
                            self.frequency_data['post']['button']['call']['calledCount'] += 1
                            self.frequency_data['post']['button']['raise']['calledCount'] += 1
                            self.frequency_data['post']['button']['raise']['called'] += 1
                        self.frequency_data['post']['button']['raise']['raised'] += 1
                        return 'raise', raise_amount, self.post_button_raise_raised_before(), 21, None
                    if hand_percentage < self.post_button_raise_raised_before() + self.post_button_call_raised_before() and can_call:
                        if is_called:
                            self.frequency_data['post']['button']['call']['calledCount'] += 1
                            self.frequency_data['post']['button']['raise']['calledCount'] += 1
                            self.frequency_data['post']['button']['call']['called'] += 1
                        self.frequency_data['post']['button']['call']['raised'] += 1
                        return 'call', call_amount, self.post_button_raise_raised_before() + self.post_button_call_raised_before(), 18, None
                    raise_id = 21
                    call_id = 18
                if is_called:
                    self.frequency_data['post']['button']['call']['calledCount'] += 1
                    self.frequency_data['post']['button']['raise']['calledCount'] += 1
                    if hand_percentage < self.post_button_raise_called_before() and can_raise:
                        if is_raised:
                            self.frequency_data['post']['button']['raise']['raised'] += 1
                        self.frequency_data['post']['button']['raise']['called'] += 1
                        return 'raise', raise_amount, self.post_button_raise_called_before(), 20, None
                    if hand_percentage < self.post_button_raise_called_before() + self.post_button_call_called_before() and can_call:
                        if is_raised:
                            self.frequency_data['post']['button']['call']['raised'] += 1
                        self.frequency_data['post']['button']['call']['called'] += 1
                        return 'call', call_amount, self.post_button_raise_called_before() + self.post_button_call_called_before(), 17, None
                    raise_id = 20
                    call_id = 17
            else:
                if not is_called and not is_raised:
                    self.frequency_data['post']['regular']['raise']['nobodyCount'] += 1
                    if hand_percentage < self.post_regular_raise_nobody_before() and can_raise:
                        self.frequency_data['post']['regular']['raise']['nobody'] += 1
                        return 'raise', raise_amount, self.post_regular_raise_nobody_before(), 14, None
                    raise_id = 14
                if is_raised:
                    self.frequency_data['post']['regular']['call']['raisedCount'] += 1
                    self.frequency_data['post']['regular']['raise']['raisedCount'] += 1
                    if hand_percentage < self.post_regular_raise_raised_before() and can_raise:
                        if is_called:
                            self.frequency_data['post']['regular']['call']['calledCount'] += 1
                            self.frequency_data['post']['regular']['raise']['calledCount'] += 1
                            self.frequency_data['post']['regular']['raise']['called'] += 1
                        self.frequency_data['post']['regular']['raise']['raised'] += 1
                        return 'raise', raise_amount, self.post_regular_raise_raised_before(), 16, None
                    if hand_percentage < self.post_regular_raise_raised_before() + self.post_regular_call_raised_before() and can_call:
                        if is_called:
                            self.frequency_data['post']['regular']['call']['calledCount'] += 1
                            self.frequency_data['post']['regular']['raise']['calledCount'] += 1
                            self.frequency_data['post']['regular']['call']['called'] += 1
                        self.frequency_data['post']['regular']['call']['raised'] += 1
                        return 'call', call_amount, self.post_regular_raise_raised_before() + self.post_regular_call_raised_before(), 13, None
                    raise_id = 16
                    call_id = 13
                if is_called:
                    self.frequency_data['post']['regular']['call']['calledCount'] += 1
                    self.frequency_data['post']['regular']['raise']['calledCount'] += 1
                    if hand_percentage < self.post_regular_raise_called_before() and can_raise:
                        if is_raised:
                            self.frequency_data['post']['regular']['raise']['raised'] += 1
                        self.frequency_data['post']['regular']['raise']['called'] += 1
                        return 'raise', raise_amount, self.post_regular_raise_called_before(), 15, None
                    if hand_percentage < self.post_regular_raise_called_before() + self.post_regular_call_called_before() and can_call:
                        if is_raised:
                            self.frequency_data['post']['regular']['call']['raised'] += 1
                        self.frequency_data['post']['regular']['call']['called'] += 1
                        return 'call', call_amount, self.post_regular_raise_called_before() + self.post_regular_call_called_before(), 12, None
                    raise_id = 15
                    call_id = 12
        other_ids = [raise_id]
        if call_id is not None:
            other_ids.append(call_id)
        return None, None, None, None, other_ids

    def receive_game_start_message(self, game_info):
        print(game_info)
        self.total_players = game_info['player_num']

    def receive_round_start_message(self, round_count, hole_card, seats):
        print(seats)
        count = 0
        for seat in seats:
            if seat['state'] == 'participating':
                count += 1
        self.number_of_players = count
        if self.number_of_players == 10:
            self.ranked_hands = STARTING_10.copy()
        if self.number_of_players == 9:
            self.ranked_hands = STARTING_9.copy()
        if self.number_of_players == 8:
            self.ranked_hands = STARTING_8.copy()
        if self.number_of_players == 7:
            self.ranked_hands = STARTING_7.copy()
        if self.number_of_players == 6:
            self.ranked_hands = STARTING_6.copy()
        if self.number_of_players == 5:
            self.ranked_hands = STARTING_5.copy()
        if self.number_of_players == 4:
            self.ranked_hands = STARTING_4.copy()
        if self.number_of_players == 3:
            self.ranked_hands = STARTING_3.copy()
        if self.number_of_players == 2:
            self.ranked_hands = STARTING_2.copy()
        self.rank_one = hole_card[0][1]
        self.rank_two = hole_card[1][1]
        self.is_suited = hole_card[0][0] == hole_card[1][0]
        self.suit_one = hole_card[0][0]
        self.suit_two = hole_card[1][0]
        self.is_all_hands_generated = False
        print(self.rank_one, self.rank_two, self.suit_one, self.suit_two, self.is_suited)

    def receive_street_start_message(self, street, round_state):
        participating = False
        for seat in round_state['seats']:
            if self.uuid == seat['uuid']:
                if seat['state'] == 'participating':
                    participating = True
                    break
        if street != 'preflop' and participating:
            community_cards = sorted(round_state['community_card'])
            if not self.is_all_hands_generated:
                hand_ranks = []
                for index, hand_data in self.ranked_hands.items():
                    if hand_data['isSuited']:
                        for suit in self.SUITS:
                            if f"{suit}{hand_data['rankOne']}" not in community_cards and f"{suit}{hand_data['rankTwo']}" not in community_cards:
                                cards = [f"{suit}{hand_data['rankOne']}", f"{suit}{hand_data['rankTwo']}"]
                                win_rate = estimate_hole_card_win_rate(100, self.number_of_players, tuple(cards), tuple(community_cards))
                                hand_ranks.append(
                                    {'winRate': win_rate, 'rankOne': hand_data['rankOne'], 'rankTwo': hand_data['rankTwo'],
                                     'isSuited': hand_data['isSuited'], 'suitOne': suit, 'suitTwo': suit})
                    else:
                        if hand_data['rankOne'] == hand_data['rankTwo']:
                            suit_list = self.PAIRED_SUITS
                        else:
                            suit_list = self.NOT_PAIRED_SUITS
                        for pairs in suit_list:
                            if f"{pairs[0]}{hand_data['rankOne']}" not in community_cards and f"{pairs[1]}{hand_data['rankTwo']}" not in community_cards:
                                cards = [f"{pairs[0]}{hand_data['rankOne']}", f"{pairs[1]}{hand_data['rankTwo']}"]
                                win_rate = estimate_hole_card_win_rate(100, self.number_of_players, tuple(cards), tuple(community_cards))
                                hand_ranks.append(
                                    {'winRate': win_rate, 'rankOne': hand_data['rankOne'], 'rankTwo': hand_data['rankTwo'],
                                     'isSuited': hand_data['isSuited'], 'suitOne': pairs[0], 'suitTwo': pairs[1]})
                hand_ranks = sorted(hand_ranks, key=lambda k: k['winRate'], reverse=True)
                self.ranked_hands = index_hand_dictionary(hand_ranks, False)
                self.is_all_hands_generated = True
            else:
                hand_ranks = []
                for index, hand_data in self.ranked_hands.items():
                    if f"{hand_data['suitOne']}{hand_data['rankOne']}" not in community_cards and f"{hand_data['suitTwo']}{hand_data['rankTwo']}" not in community_cards:
                        cards = [f"{hand_data['suitOne']}{hand_data['rankOne']}", f"{hand_data['suitTwo']}{hand_data['rankTwo']}"]
                        win_rate = estimate_hole_card_win_rate(100, self.number_of_players, tuple(cards), tuple(community_cards))
                        hand_ranks.append(
                            {'winRate': win_rate, 'rankOne': hand_data['rankOne'], 'rankTwo': hand_data['rankTwo'],
                             'isSuited': hand_data['isSuited'], 'suitOne': hand_data['suitOne'], 'suitTwo': hand_data['suitTwo']})
                hand_ranks = sorted(hand_ranks, key=lambda k: k['winRate'], reverse=True)
                self.ranked_hands = index_hand_dictionary(hand_ranks, False)

            print('interesting', round_state)

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        print('finish', round_state)
        point = [
            safe_div(self.frequency_data['pre']['regular']['call']['nobody'], self.frequency_data['pre']['regular']['call']['nobodyCount']),
            safe_div(self.frequency_data['pre']['regular']['call']['called'], self.frequency_data['pre']['regular']['call']['calledCount']),
            safe_div(self.frequency_data['pre']['regular']['call']['raised'], self.frequency_data['pre']['regular']['call']['raisedCount']),
            safe_div(self.frequency_data['pre']['regular']['raise']['nobody'], self.frequency_data['pre']['regular']['raise']['nobodyCount']),
            safe_div(self.frequency_data['pre']['regular']['raise']['called'], self.frequency_data['pre']['regular']['raise']['calledCount']),
            safe_div(self.frequency_data['pre']['regular']['raise']['raised'], self.frequency_data['pre']['regular']['raise']['raisedCount']),
            safe_div(self.frequency_data['pre']['button']['call']['nobody'], self.frequency_data['pre']['button']['call']['nobodyCount']),
            safe_div(self.frequency_data['pre']['button']['call']['called'], self.frequency_data['pre']['button']['call']['calledCount']),
            safe_div(self.frequency_data['pre']['button']['call']['raised'], self.frequency_data['pre']['button']['call']['raisedCount']),
            safe_div(self.frequency_data['pre']['button']['raise']['nobody'], self.frequency_data['pre']['button']['raise']['nobodyCount']),
            safe_div(self.frequency_data['pre']['button']['raise']['called'], self.frequency_data['pre']['button']['raise']['calledCount']),
            safe_div(self.frequency_data['pre']['button']['raise']['raised'], self.frequency_data['pre']['button']['raise']['raisedCount']),
            safe_div(self.frequency_data['post']['regular']['call']['called'], self.frequency_data['post']['regular']['call']['calledCount']),
            safe_div(self.frequency_data['post']['regular']['call']['raised'], self.frequency_data['post']['regular']['call']['raisedCount']),
            safe_div(self.frequency_data['post']['regular']['raise']['nobody'], self.frequency_data['post']['regular']['raise']['nobodyCount']),
            safe_div(self.frequency_data['post']['regular']['raise']['called'], self.frequency_data['post']['regular']['raise']['calledCount']),
            safe_div(self.frequency_data['post']['regular']['raise']['raised'], self.frequency_data['post']['regular']['raise']['raisedCount']),
            safe_div(self.frequency_data['post']['button']['call']['called'], self.frequency_data['post']['button']['call']['calledCount']),
            safe_div(self.frequency_data['post']['button']['call']['raised'], self.frequency_data['post']['button']['call']['raisedCount']),
            safe_div(self.frequency_data['post']['button']['raise']['nobody'], self.frequency_data['post']['button']['raise']['nobodyCount']),
            safe_div(self.frequency_data['post']['button']['raise']['called'], self.frequency_data['post']['button']['raise']['calledCount']),
            safe_div(self.frequency_data['post']['button']['raise']['raised'], self.frequency_data['post']['button']['raise']['raisedCount']),
        ]
        point_weka = []
        for phase, positionData in self.attr_names.items():
            for position, actionData in positionData.items():
                for action, stateData in actionData.items():
                    for state in stateData:
                        if self.frequency_data[phase][position][action][state] == 0 or \
                                self.frequency_data[phase][position][action][f'{state}Count'] == 0 or (
                                self.frequency_data[phase][position][action][state] ==
                                self.frequency_data[phase][position][action][f'{state}Count']):
                            point_weka.append(float("NaN"))
                        else:
                            point_weka.append(safe_div(self.frequency_data[phase][position][action][state], self.frequency_data[phase][position][action][f'{state}Count']))
        point_weka.append(float("NaN"))
        inst = Instance.create_instance(point_weka)
        inst.dataset = self.ada_data
        dist_a = self.ada_classifier.distribution_for_instance(inst)
        self.ada_history.append(dist_a.tolist())
        inst.dataset = self.multilayer_data
        dist_m = self.multilayer_classifier.distribution_for_instance(inst)
        self.multilayer_history.append(dist_m.tolist())
        inst.dataset = self.bayes_net_data
        dist_b = self.bayes_net_classifier.distribution_for_instance(inst)
        self.bayes_net_history.append(dist_b.tolist())
        distances = []
        for center in self.centers:
            distances.append(dist(center, point))
        self.distances.append(distances)
        print(self.distances)
        print(self.probabilities_history)
        print(self.ada_history)
        print(self.multilayer_history)
        print(self.bayes_net_history)
        with open(f"H:\\Universitate\\meta_data\\results\\{self.name}", "w") as fp:
            fp.write(str(self.distances))
            fp.write("\n\n")
            fp.write(str(self.probabilities_history))
            fp.write("\n\n")
            fp.write(str(self.ada_history))
            fp.write("\n\n")
            fp.write(str(self.multilayer_history))
            fp.write("\n\n")
            fp.write(str(self.bayes_net_history))
            fp.write("\n\n")

    def pre_regular_call_nobody_before(self):
        err_msg = self.__build_err_msg("pre_regular_call_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_regular_call_called_before(self):
        err_msg = self.__build_err_msg("pre_regular_call_called_before")
        raise NotImplementedError(err_msg)

    def pre_regular_call_raised_before(self):
        err_msg = self.__build_err_msg("pre_regular_call_raised_before")
        raise NotImplementedError(err_msg)

    def pre_regular_raise_nobody_before(self):
        err_msg = self.__build_err_msg("pre_regular_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_regular_raise_called_before(self):
        err_msg = self.__build_err_msg("pre_regular_raise_called_before")
        raise NotImplementedError(err_msg)

    def pre_regular_raise_raised_before(self):
        err_msg = self.__build_err_msg("pre_regular_raise_raised_before")
        raise NotImplementedError(err_msg)

    def pre_button_call_nobody_before(self):
        err_msg = self.__build_err_msg("pre_button_call_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_button_call_called_before(self):
        err_msg = self.__build_err_msg("pre_button_call_called_before")
        raise NotImplementedError(err_msg)

    def pre_button_call_raised_before(self):
        err_msg = self.__build_err_msg("pre_button_call_raised_before")
        raise NotImplementedError(err_msg)

    def pre_button_raise_nobody_before(self):
        err_msg = self.__build_err_msg("pre_button_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_button_raise_called_before(self):
        err_msg = self.__build_err_msg("pre_button_raise_called_before")
        raise NotImplementedError(err_msg)

    def pre_button_raise_raised_before(self):
        err_msg = self.__build_err_msg("pre_button_raise_raised_before")
        raise NotImplementedError(err_msg)

    def post_regular_call_called_before(self):
        err_msg = self.__build_err_msg("post_regular_call_called_before")
        raise NotImplementedError(err_msg)

    def post_regular_call_raised_before(self):
        err_msg = self.__build_err_msg("post_regular_call_raised_before")
        raise NotImplementedError(err_msg)

    def post_regular_raise_nobody_before(self):
        err_msg = self.__build_err_msg("post_regular_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def post_regular_raise_called_before(self):
        err_msg = self.__build_err_msg("post_regular_raise_called_before")
        raise NotImplementedError(err_msg)

    def post_regular_raise_raised_before(self):
        err_msg = self.__build_err_msg("post_regular_raise_raised_before")
        raise NotImplementedError(err_msg)

    def post_button_call_called_before(self):
        err_msg = self.__build_err_msg("post_button_call_called_before")
        raise NotImplementedError(err_msg)

    def post_button_call_raised_before(self):
        err_msg = self.__build_err_msg("post_button_call_raised_before")
        raise NotImplementedError(err_msg)

    def post_button_raise_nobody_before(self):
        err_msg = self.__build_err_msg("post_button_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def post_button_raise_called_before(self):
        err_msg = self.__build_err_msg("post_button_raise_called_before")
        raise NotImplementedError(err_msg)

    def post_button_raise_raised_before(self):
        err_msg = self.__build_err_msg("post_button_raise_raised_before")
        raise NotImplementedError(err_msg)

    def __build_err_msg(self, msg):
        return "Your client does not implement [ {0} ] method".format(msg)
