import pandas as pd
import matplotlib.pyplot as plt
from collections import defaultdict
import json

data = []
correct_id = 2
with open(f"H:\\Universitate\\meta_data\\results\\4players\\c{correct_id}.txt", 'r') as f:
    for line in f:
        if line != "\n":
            data.append(json.loads(line))
first_correct = [None, None, None, None, None]
correct_after = [0, 0, 0, 0, 0]
total_after = [0, 0, 0, 0, 0]
preparedData = defaultdict(dict)
for x, types in enumerate(data):
    for round_number, values in enumerate(types):
        preparedData[x]['x_values'] = range(len(types))
        min_value = 4
        min_value_id = None
        max_value = 0
        max_value_id = None
        for i, value in enumerate(values):
            if value > max_value:
                max_value = value
                max_value_id = i
            if value < min_value:
                min_value = value
                min_value_id = i
            if str(i) in preparedData[x]:
                preparedData[x][str(i)].append(value)
            else:
                preparedData[x][str(i)] = [value]
        correct = False
        if x == 0:
            if min_value_id == correct_id:
                correct = True
                if first_correct[x] is None:
                    first_correct[x] = round_number
        else:
            if max_value_id == correct_id:
                correct = True
                if first_correct[x] is None:
                    first_correct[x] = round_number
        if first_correct[x] is not None:
            total_after[x] += 1
            if correct:
                correct_after[x] += 1

for x in range(5):
    print("index:", x)
    print(first_correct[x])
    if total_after[x] == 0:
        print(0.0)
    else:
        print(correct_after[x] / total_after[x])

for type_id, type_data in preparedData.items():
    print(type_data)
    df = pd.DataFrame(type_data)
    # multiple line plots
    plt.plot('x_values', '0', data=df, color='black', linewidth=4)
    plt.plot('x_values', '1', data=df, linewidth=2)
    plt.plot('x_values', '2', data=df, linewidth=2)
    # show legend
    plt.legend()

    # show graph
    plt.show()
