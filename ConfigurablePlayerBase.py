from pypokerengine.players import BasePokerPlayer
from calculatedStartingCardRanks import *
from HelperFunctions import index_hand_dictionary, find_percentage_for_hand, find_percentage_for_specific_hand, estimate_hole_card_win_rate


# This is the base class that should be extended to create AI player that plays according to given
# probabilities in defined situations

class ConfigurablePlayerBase(BasePokerPlayer):
    SUITS = ['C', 'D', 'H', 'S']
    PAIRED_SUITS = [['C', 'D'], ['C', 'H'], ['C', 'S'], ['D', 'H'], ['D', 'S'], ['H', 'S']]
    NOT_PAIRED_SUITS = [['C', 'D'], ['C', 'H'], ['C', 'S'], ['D', 'H'], ['D', 'S'], ['H', 'S'], ['D', 'C'], ['H', 'C'], ['S', 'C'], ['H', 'D'], ['S', 'D'], ['S', 'H']]

    def __init__(self):
        super().__init__()
        self.total_players = 10
        self.number_of_players = 10
        self.ranked_hands = STARTING_10.copy()
        self.rank_one = ''
        self.rank_two = ''
        self.suit_one = ''
        self.suit_two = ''
        self.is_suited = False
        self.is_all_hands_generated = False

    def declare_action(self, valid_actions, hole_card, round_state):
        print(valid_actions)
        print(round_state)
        can_raise = False
        can_call = False
        call_amount = 0
        raise_amount = 0
        for action in valid_actions:
            if action['action'] == 'call':
                can_call = True
                call_amount = action['amount']
            if action['action'] == 'raise' and action['amount']['min'] > 0:
                can_raise = True
                raise_amount = action['amount']['min']
        print(round_state['street'])

        if round_state['street'] == 'preflop':
            hand_percentage = find_percentage_for_hand(self.ranked_hands, self.rank_one, self.rank_two, self.is_suited)
        else:
            hand_percentage = find_percentage_for_specific_hand(self.ranked_hands, self.rank_one, self.rank_two, self.suit_one, self.suit_two)

        is_raised = False
        is_called = False
        button_or_last = False
        if round_state['street'] in round_state['action_histories']:
            for action in round_state['action_histories'][round_state['street']]:
                if action['action'] == 'CALL' and action['amount'] > 0:
                    is_called = True
                if action['action'] == 'RAISE':
                    is_raised = True

        dealer_button = round_state['dealer_btn']
        while True:
            if round_state['seats'][dealer_button]['state'] != 'folded':
                if round_state['seats'][dealer_button]['uuid'] == self.uuid:
                    button_or_last = True
                break
            dealer_button -= 1
            if dealer_button < 0:
                dealer_button = self.total_players - 1
        print(hand_percentage, button_or_last, is_called, is_raised, self.ranked_hands)
        # return 'call', call_amount
        is_preflop = round_state['street'] == 'preflop'
        action, amount, percentage = self.get_action_amount_percentage(is_preflop, is_called, is_raised, button_or_last, can_raise, can_call, hand_percentage, call_amount, raise_amount)
        if action:
            largest_index = max(self.ranked_hands)
            cutoff_point = round(largest_index * percentage)
            while cutoff_point not in self.ranked_hands:
                cutoff_point += 1
            for key in self.ranked_hands.copy():
                if key > cutoff_point:
                    del self.ranked_hands[key]

            print(action, amount)
            return action, amount

        if can_call and call_amount == 0:
            print('call', 0)
            return 'call', 0
        else:
            print('fold', 0)
            return 'fold', 0

    def get_action_amount_percentage(self, is_preflop, is_called, is_raised, button_or_last, can_raise, can_call, hand_percentage, call_amount, raise_amount):
        if is_preflop:
            if button_or_last:
                if not is_called and not is_raised:
                    if hand_percentage < self.pre_button_raise_nobody_before() and can_raise:
                        return 'raise', raise_amount, self.pre_button_raise_nobody_before()
                    if hand_percentage < self.pre_button_raise_nobody_before() + self.pre_button_call_nobody_before() and can_call:
                        return 'call', call_amount, self.pre_button_raise_nobody_before() + self.pre_button_call_nobody_before()
                if is_raised:
                    if hand_percentage < self.pre_button_raise_raised_before() and can_raise:
                        return 'raise', raise_amount, self.pre_button_raise_raised_before()
                    if hand_percentage < self.pre_button_raise_raised_before() + self.pre_button_call_raised_before() and can_call:
                        return 'call', call_amount, self.pre_button_raise_raised_before() + self.pre_button_call_raised_before()
                if is_called:
                    if hand_percentage < self.pre_button_raise_called_before() and can_raise:
                        return 'raise', raise_amount, self.pre_button_raise_called_before()
                    if hand_percentage < self.pre_button_raise_called_before() + self.pre_button_call_called_before() and can_call:
                        return 'call', call_amount, self.pre_button_raise_called_before() + self.pre_button_call_called_before()
            else:
                if not is_called and not is_raised:
                    if hand_percentage < self.pre_regular_raise_nobody_before() and can_raise:
                        return 'raise', raise_amount, self.pre_regular_raise_nobody_before()
                    if hand_percentage < self.pre_regular_raise_nobody_before() + self.pre_regular_call_nobody_before() and can_call:
                        return 'call', call_amount, self.pre_regular_raise_nobody_before() + self.pre_regular_call_nobody_before()
                if is_raised:
                    if hand_percentage < self.pre_regular_raise_raised_before() and can_raise:
                        return 'raise', raise_amount, self.pre_regular_raise_raised_before()
                    if hand_percentage < self.pre_regular_raise_raised_before() + self.pre_regular_call_raised_before() and can_call:
                        return 'call', call_amount, self.pre_regular_raise_raised_before() + self.pre_regular_call_raised_before()
                if is_called:
                    if hand_percentage < self.pre_regular_raise_called_before() and can_raise:
                        return 'raise', raise_amount, self.pre_regular_raise_called_before()
                    if hand_percentage < self.pre_regular_raise_called_before() + self.pre_regular_call_called_before() and can_call:
                        return 'call', call_amount, self.pre_regular_raise_called_before() + self.pre_regular_call_called_before()
        else:
            if button_or_last:
                if not is_called and not is_raised:
                    if hand_percentage < self.post_button_raise_nobody_before() and can_raise:
                        return 'raise', raise_amount, self.post_button_raise_nobody_before()
                if is_raised:
                    if hand_percentage < self.post_button_raise_raised_before() and can_raise:
                        return 'raise', raise_amount, self.post_button_raise_raised_before()
                    if hand_percentage < self.post_button_raise_raised_before() + self.post_button_call_raised_before() and can_call:
                        return 'call', call_amount, self.post_button_raise_raised_before() + self.post_button_call_raised_before()
                if is_called:
                    if hand_percentage < self.post_button_raise_called_before() and can_raise:
                        return 'raise', raise_amount, self.post_button_raise_called_before()
                    if hand_percentage < self.post_button_raise_called_before() + self.post_button_call_called_before() and can_call:
                        return 'call', call_amount, self.post_button_raise_called_before() + self.post_button_call_called_before()
            else:
                if not is_called and not is_raised:
                    if hand_percentage < self.post_regular_raise_nobody_before() and can_raise:
                        return 'raise', raise_amount, self.post_regular_raise_nobody_before()
                if is_raised:
                    if hand_percentage < self.post_regular_raise_raised_before() and can_raise:
                        return 'raise', raise_amount, self.post_regular_raise_raised_before()
                    if hand_percentage < self.post_regular_raise_raised_before() + self.post_regular_call_raised_before() and can_call:
                        return 'call', call_amount, self.post_regular_raise_raised_before() + self.post_regular_call_raised_before()
                if is_called:
                    if hand_percentage < self.post_regular_raise_called_before() and can_raise:
                        return 'raise', raise_amount, self.post_regular_raise_called_before()
                    if hand_percentage < self.post_regular_raise_called_before() + self.post_regular_call_called_before() and can_call:
                        return 'call', call_amount, self.post_regular_raise_called_before() + self.post_regular_call_called_before()
        return None, None, None

    def receive_game_start_message(self, game_info):
        print(game_info)
        self.total_players = game_info['player_num']

    def receive_round_start_message(self, round_count, hole_card, seats):
        print(seats)
        count = 0
        for seat in seats:
            if seat['state'] == 'participating':
                count += 1
        self.number_of_players = count
        if self.number_of_players == 10:
            self.ranked_hands = STARTING_10.copy()
        if self.number_of_players == 9:
            self.ranked_hands = STARTING_9.copy()
        if self.number_of_players == 8:
            self.ranked_hands = STARTING_8.copy()
        if self.number_of_players == 7:
            self.ranked_hands = STARTING_7.copy()
        if self.number_of_players == 6:
            self.ranked_hands = STARTING_6.copy()
        if self.number_of_players == 5:
            self.ranked_hands = STARTING_5.copy()
        if self.number_of_players == 4:
            self.ranked_hands = STARTING_4.copy()
        if self.number_of_players == 3:
            self.ranked_hands = STARTING_3.copy()
        if self.number_of_players == 2:
            self.ranked_hands = STARTING_2.copy()
        self.rank_one = hole_card[0][1]
        self.rank_two = hole_card[1][1]
        self.is_suited = hole_card[0][0] == hole_card[1][0]
        self.suit_one = hole_card[0][0]
        self.suit_two = hole_card[1][0]
        self.is_all_hands_generated = False
        print(self.rank_one, self.rank_two, self.suit_one, self.suit_two, self.is_suited)

    def receive_street_start_message(self, street, round_state):
        participating = False
        for seat in round_state['seats']:
            if self.uuid == seat['uuid']:
                if seat['state'] == 'participating':
                    participating = True
                    break
        if street != 'preflop' and participating:
            community_cards = sorted(round_state['community_card'])
            if not self.is_all_hands_generated:
                hand_ranks = []
                for index, hand_data in self.ranked_hands.items():
                    if hand_data['isSuited']:
                        for suit in self.SUITS:
                            if f"{suit}{hand_data['rankOne']}" not in community_cards and f"{suit}{hand_data['rankTwo']}" not in community_cards:
                                cards = [f"{suit}{hand_data['rankOne']}", f"{suit}{hand_data['rankTwo']}"]
                                win_rate = estimate_hole_card_win_rate(100, self.number_of_players, tuple(cards), tuple(community_cards))
                                hand_ranks.append(
                                    {'winRate': win_rate, 'rankOne': hand_data['rankOne'], 'rankTwo': hand_data['rankTwo'],
                                     'isSuited': hand_data['isSuited'], 'suitOne': suit, 'suitTwo': suit})
                    else:
                        if hand_data['rankOne'] == hand_data['rankTwo']:
                            suit_list = self.PAIRED_SUITS
                        else:
                            suit_list = self.NOT_PAIRED_SUITS
                        for pairs in suit_list:
                            if f"{pairs[0]}{hand_data['rankOne']}" not in community_cards and f"{pairs[1]}{hand_data['rankTwo']}" not in community_cards:
                                cards = [f"{pairs[0]}{hand_data['rankOne']}", f"{pairs[1]}{hand_data['rankTwo']}"]
                                win_rate = estimate_hole_card_win_rate(100, self.number_of_players, tuple(cards), tuple(community_cards))
                                hand_ranks.append(
                                    {'winRate': win_rate, 'rankOne': hand_data['rankOne'], 'rankTwo': hand_data['rankTwo'],
                                     'isSuited': hand_data['isSuited'], 'suitOne': pairs[0], 'suitTwo': pairs[1]})
                hand_ranks = sorted(hand_ranks, key=lambda k: k['winRate'], reverse=True)
                self.ranked_hands = index_hand_dictionary(hand_ranks, False)
                self.is_all_hands_generated = True
            else:
                hand_ranks = []
                for index, hand_data in self.ranked_hands.items():
                    if f"{hand_data['suitOne']}{hand_data['rankOne']}" not in community_cards and f"{hand_data['suitTwo']}{hand_data['rankTwo']}" not in community_cards:
                        cards = [f"{hand_data['suitOne']}{hand_data['rankOne']}", f"{hand_data['suitTwo']}{hand_data['rankTwo']}"]
                        win_rate = estimate_hole_card_win_rate(100, self.number_of_players, tuple(cards), tuple(community_cards))
                        hand_ranks.append(
                            {'winRate': win_rate, 'rankOne': hand_data['rankOne'], 'rankTwo': hand_data['rankTwo'],
                             'isSuited': hand_data['isSuited'], 'suitOne': hand_data['suitOne'], 'suitTwo': hand_data['suitTwo']})
                hand_ranks = sorted(hand_ranks, key=lambda k: k['winRate'], reverse=True)
                self.ranked_hands = index_hand_dictionary(hand_ranks, False)

            print('interesting', round_state)

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        print('finish', round_state)

    def pre_regular_call_nobody_before(self):
        err_msg = self.__build_err_msg("pre_regular_call_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_regular_call_called_before(self):
        err_msg = self.__build_err_msg("pre_regular_call_called_before")
        raise NotImplementedError(err_msg)

    def pre_regular_call_raised_before(self):
        err_msg = self.__build_err_msg("pre_regular_call_raised_before")
        raise NotImplementedError(err_msg)

    def pre_regular_raise_nobody_before(self):
        err_msg = self.__build_err_msg("pre_regular_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_regular_raise_called_before(self):
        err_msg = self.__build_err_msg("pre_regular_raise_called_before")
        raise NotImplementedError(err_msg)

    def pre_regular_raise_raised_before(self):
        err_msg = self.__build_err_msg("pre_regular_raise_raised_before")
        raise NotImplementedError(err_msg)

    def pre_button_call_nobody_before(self):
        err_msg = self.__build_err_msg("pre_button_call_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_button_call_called_before(self):
        err_msg = self.__build_err_msg("pre_button_call_called_before")
        raise NotImplementedError(err_msg)

    def pre_button_call_raised_before(self):
        err_msg = self.__build_err_msg("pre_button_call_raised_before")
        raise NotImplementedError(err_msg)

    def pre_button_raise_nobody_before(self):
        err_msg = self.__build_err_msg("pre_button_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def pre_button_raise_called_before(self):
        err_msg = self.__build_err_msg("pre_button_raise_called_before")
        raise NotImplementedError(err_msg)

    def pre_button_raise_raised_before(self):
        err_msg = self.__build_err_msg("pre_button_raise_raised_before")
        raise NotImplementedError(err_msg)

    def post_regular_call_called_before(self):
        err_msg = self.__build_err_msg("post_regular_call_called_before")
        raise NotImplementedError(err_msg)

    def post_regular_call_raised_before(self):
        err_msg = self.__build_err_msg("post_regular_call_raised_before")
        raise NotImplementedError(err_msg)

    def post_regular_raise_nobody_before(self):
        err_msg = self.__build_err_msg("post_regular_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def post_regular_raise_called_before(self):
        err_msg = self.__build_err_msg("post_regular_raise_called_before")
        raise NotImplementedError(err_msg)

    def post_regular_raise_raised_before(self):
        err_msg = self.__build_err_msg("post_regular_raise_raised_before")
        raise NotImplementedError(err_msg)

    def post_button_call_called_before(self):
        err_msg = self.__build_err_msg("post_button_call_called_before")
        raise NotImplementedError(err_msg)

    def post_button_call_raised_before(self):
        err_msg = self.__build_err_msg("post_button_call_raised_before")
        raise NotImplementedError(err_msg)

    def post_button_raise_nobody_before(self):
        err_msg = self.__build_err_msg("post_button_raise_nobody_before")
        raise NotImplementedError(err_msg)

    def post_button_raise_called_before(self):
        err_msg = self.__build_err_msg("post_button_raise_called_before")
        raise NotImplementedError(err_msg)

    def post_button_raise_raised_before(self):
        err_msg = self.__build_err_msg("post_button_raise_raised_before")
        raise NotImplementedError(err_msg)

    def __build_err_msg(self, msg):
        return "Your client does not implement [ {0} ] method".format(msg)
