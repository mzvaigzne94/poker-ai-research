import os
import matplotlib.pyplot as plt

directories = ["H:\\Universitate\\data\\h1-nobots\\", "H:\\Universitate\\data\\holdem\\",
               "H:\\Universitate\\data\\holdem1\\", "H:\\Universitate\\data\\holdem2\\",
               "H:\\Universitate\\data\\holdem3\\", "H:\\Universitate\\data\\holdemii\\"]

counts = {
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0,
    9: 0,
    10: 0,
    11: 0,
    12: 0,
}

for directory in directories:
    subdirectories = [dI for dI in os.listdir(directory) if os.path.isdir(os.path.join(directory, dI))]
    for subdirectory in subdirectories:
        fullPath = f'{directory}{subdirectory}\\hroster'
        with open(fullPath, 'r') as hrosterFile:
            for line in hrosterFile:
                try:
                    array = line.strip().split()
                    player_count = int(array[1])
                    if player_count < 30:
                        counts[player_count] += 1
                except KeyError:
                    print(line)
                except IndexError:
                    print(line)

preparedData = {
    'x_values': range(2, 13),
    'y_values': []
}

for count, total in counts.items():
    preparedData['y_values'].append(total)

print(counts)
print(preparedData)

plt.bar(preparedData['x_values'], preparedData['y_values'], align='center')
plt.ticklabel_format(style='plain')
plt.ylabel('Izspēļu skaits')
plt.xlabel('Spēlētāju skaits')
plt.show()

