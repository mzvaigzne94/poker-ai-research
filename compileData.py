import os

# directory = "H:\\Universitate\\data\\h1-nobots\\"
# directory = "H:\\Universitate\\data\\holdem\\"
# directory = "H:\\Universitate\\data\\holdem1\\"
# directory = "H:\\Universitate\\data\\holdem2\\"
directory = "H:\\Universitate\\data\\holdem3\\"
# directory = "H:\\Universitate\\data\\holdemii\\"

output_filename2 = f'{directory}output2.arff'
output_filename3 = f'{directory}output3.arff'
output_filename4 = f'{directory}output4.arff'
output_filename5 = f'{directory}output5.arff'
output_filename6 = f'{directory}output6.arff'
output_filename7 = f'{directory}output7.arff'
output_filename8 = f'{directory}output8.arff'
output_filename9 = f'{directory}output9.arff'
output_filename10 = f'{directory}output10.arff'
with open(output_filename2, "w") as fp2, open(output_filename3, "w") as fp3, open(output_filename4, "w") as fp4, open(
        output_filename5, "w") as fp5, open(output_filename6, "w") as fp6, open(output_filename7, "w") as fp7, open(
        output_filename8, "w") as fp8, open(output_filename9, "w") as fp9, open(output_filename10, "w") as fp10:
    files = {2: fp2, 3: fp3, 4: fp4, 5: fp5, 6: fp6, 7: fp7, 8: fp8, 9: fp9, 10: fp10}
    for playerCount, file in files.items():
        file.write('''@RELATION poker

@ATTRIBUTE pre_regular_call_nobody_before numeric
@ATTRIBUTE pre_regular_call_called_before numeric
@ATTRIBUTE pre_regular_call_raised_before numeric
@ATTRIBUTE pre_regular_raise_nobody_before numeric
@ATTRIBUTE pre_regular_raise_called_before numeric
@ATTRIBUTE pre_regular_raise_raised_before numeric
@ATTRIBUTE pre_button_call_nobody_before numeric
@ATTRIBUTE pre_button_call_called_before numeric
@ATTRIBUTE pre_button_call_raised_before numeric
@ATTRIBUTE pre_button_raise_nobody_before numeric
@ATTRIBUTE pre_button_raise_called_before numeric
@ATTRIBUTE pre_button_raise_raised_before numeric
@ATTRIBUTE post_regular_call_nobody_before numeric
@ATTRIBUTE post_regular_call_called_before numeric
@ATTRIBUTE post_regular_call_raised_before numeric
@ATTRIBUTE post_regular_raise_nobody_before numeric
@ATTRIBUTE post_regular_raise_called_before numeric
@ATTRIBUTE post_regular_raise_raised_before numeric
@ATTRIBUTE post_button_call_nobody_before numeric
@ATTRIBUTE post_button_call_called_before numeric
@ATTRIBUTE post_button_call_raised_before numeric
@ATTRIBUTE post_button_raise_nobody_before numeric
@ATTRIBUTE post_button_raise_called_before numeric
@ATTRIBUTE post_button_raise_raised_before numeric

@DATA
''')
    subdirectories = [dI for dI in os.listdir(directory) if os.path.isdir(os.path.join(directory, dI))]

    for subdirectory in subdirectories:
        fullPath = f'{directory}{subdirectory}\\'
        for playerCount, file in files.items():
            with open(f'{fullPath}output{playerCount}.arff', 'r') as arffFile:
                for i in range(28):
                    arffFile.readline()
                for line in arffFile:
                    file.write(line)
