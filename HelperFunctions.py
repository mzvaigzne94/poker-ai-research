from treys import Card, Deck, Evaluator
from functools import lru_cache


def convert_card(card):
    mapping = {'C': 'c', 'D': 'd', 'H': 'h', 'S': 's'}
    return f"{card[1]}{mapping[card[0]]}"


@lru_cache(maxsize=None)
def estimate_hole_card_win_rate(simulations, player_count, hole_cards, community_cards):
    evaluator = Evaluator()
    cards_to_add = 5
    base_board = []
    base_deck = Deck()
    for card in community_cards:
        converted_card = convert_card(card)
        base_deck.remove(converted_card)
        base_board.append(Card.new(converted_card))
        cards_to_add -= 1
    hand = []
    for card in hole_cards:
        converted_card = convert_card(card)
        base_deck.remove(converted_card)
        hand.append(Card.new(converted_card))
    base_cards = base_deck.cards
    loss_count = 0
    for _ in range(simulations):
        board = base_board.copy()
        deck = Deck()
        deck.cards = base_cards.copy()
        deck.shuffle_cards()
        for __ in range(cards_to_add):
            board.append(deck.draw())
        rank = evaluator.evaluate(board, hand)
        for ___ in range(player_count-1):
            opponent_cards = deck.draw(2)
            opponent_rank = evaluator.evaluate(board, opponent_cards)
            if opponent_rank < rank:
                loss_count += 1
                break
    return (simulations - loss_count) / simulations


def index_hand_dictionary(hands, use_short_count):
    hands = sorted(hands, key=lambda k: k['winRate'], reverse=True)
    result = {}
    count = 0
    for handDict in hands:
        if use_short_count:
            if handDict['rankOne'] == handDict['rankTwo']:
                count += 6
            elif handDict['isSuited']:
                count += 4
            else:
                count += 12
        else:
            count += 1
        result[count] = handDict
    return result


def find_percentage_for_hand(hands, rank_one, rank_two, is_suited):
    largest_index = max(hands)
    previous_index = 0
    for index, data in hands.items():
        if ((data['rankOne'] == rank_one and data['rankTwo'] == rank_two) or (
                data['rankOne'] == rank_two and data['rankTwo'] == rank_one)) and data['isSuited'] == is_suited:
            return previous_index / largest_index
        previous_index = index

    return 0


def find_percentage_for_specific_hand(hands, rank_one, rank_two, suit_one, suit_two):
    largest_index = max(hands)
    previous_index = 0
    for index, data in hands.items():
        if (data['rankOne'] == rank_one and data['rankTwo'] == rank_two and data['suitOne'] == suit_one and data['suitTwo'] == suit_two) or (
                data['rankOne'] == rank_two and data['rankTwo'] == rank_one and data['suitOne'] == suit_two and data['suitTwo'] == suit_one):
            return previous_index / largest_index
        previous_index = index

    return 0


def safe_div(x, y):
    if y == 0:
        return 0
    return x / y
