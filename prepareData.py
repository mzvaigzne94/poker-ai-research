import os
from collections import defaultdict

# directory = "H:\\Universitate\\data\\h1-nobots\\"
# directory = "H:\\Universitate\\data\\holdem\\"
# directory = "H:\\Universitate\\data\\holdem1\\"
# directory = "H:\\Universitate\\data\\holdem2\\"
# directory = "H:\\Universitate\\data\\holdem3\\"
directory = "H:\\Universitate\\data\\holdemii\\"

subdirectories = [dI for dI in os.listdir(directory) if os.path.isdir(os.path.join(directory, dI))]

for subdirectory in subdirectories:
    fullPath = f'{directory}{subdirectory}\\'
    roster = open(f'{fullPath}hroster', 'r')
    Lines = roster.readlines()
    games = {}
    players = defaultdict(dict)

    for line in Lines:
        array = line.strip().split()
        try:
            if int(array[1]) < 11:
                games[array[0]] = array[2:]
        except IndexError:
            print(array)

    directoryName = f'{fullPath}pdb'
    for filename in os.listdir(directoryName):
        with open(os.path.join(directoryName, filename), 'r') as f:
            playerLines = f.readlines()
            for line in playerLines:
                array = line.strip().split()
                try:
                    players[array[0]][array[1]] = {'name': array[0], 'position': int(array[3]), 'preflop': array[4],
                                                   'flop': array[5],
                                                   'turn': array[6], 'river': array[7]}
                except KeyError:
                    print(array)
                except IndexError:
                    print(array)
                except ValueError:
                    print(array)

    # player counts 3-10
    playerCounts = [*range(3, 11, 1)]
    finalData = defaultdict(dict)
    for gameId, names in games.items():
        gameData = []
        playerCount = 0
        positionsPlaying = []
        for name in games[gameId.__str__()]:
            try:
                gameData.append(players[name][gameId.__str__()])
            except KeyError:
                gameData = []
                break
            playerCount += 1
            positionsPlaying.append(playerCount)

            if name not in finalData.keys():
                for playerNumber in playerCounts:
                    finalData[name][playerNumber] = {
                        'pre': {
                            'regular': {
                                'call': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                },
                                'raise': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                }
                            },
                            'button': {
                                'call': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                },
                                'raise': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                }
                            }
                        },
                        'post': {
                            'regular': {
                                'call': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                },
                                'raise': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                }
                            },
                            'button': {
                                'call': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                },
                                'raise': {
                                    'nobody': 0,
                                    'nobodyCount': 0,
                                    'called': 0,
                                    'calledCount': 0,
                                    'raised': 0,
                                    'raisedCount': 0
                                }
                            }
                        }
                    }

        if not gameData:
            continue
        gameDataSorted = sorted(gameData, key=lambda k: k['position'])
        phases = ['preflop', 'flop', 'turn', 'river']
        for phase in phases:
            playersProcessed = 0
            isCalled = False
            isRaised = False
            if phase == 'preflop':
                prefix = 'pre'
            else:
                prefix = 'post'

            while playersProcessed != playerCount:
                for game in gameDataSorted:
                    action = None
                    if game[phase]:
                        action = game[phase][0]

                    if int(game['position']) == positionsPlaying[-1]:
                        positionText = 'button'
                    else:
                        positionText = 'regular'

                    if action == 'r' or action == 'A' or action == 'b':
                        if not isCalled and not isRaised:
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['nobody'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['nobodyCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['call']['nobodyCount'] += 1

                        if isCalled:
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['called'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['calledCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['call']['calledCount'] += 1

                        if isRaised:
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['raised'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['raisedCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['call']['raisedCount'] += 1

                        isRaised = True

                    if action == 'c':
                        if not isCalled and not isRaised:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['nobody'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['call']['nobodyCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['nobodyCount'] += 1

                        if isCalled:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['called'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['call']['calledCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['calledCount'] += 1

                        if isRaised:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['raised'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['call']['raisedCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['raisedCount'] += 1

                        isCalled = True

                    if action == 'k':
                        if isCalled:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['calledCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['calledCount'] += 1
                        else:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['nobodyCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['nobodyCount'] += 1

                    if action == 'f':
                        if not isCalled and not isRaised:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['nobodyCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['nobodyCount'] += 1

                        if isCalled:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['calledCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['calledCount'] += 1

                        if isRaised:
                            finalData[game['name']][playerCount][prefix][positionText]['call']['raisedCount'] += 1
                            finalData[game['name']][playerCount][prefix][positionText]['raise']['raisedCount'] += 1
                        positionsPlaying.remove(game['position'])

                    if game[phase] is not None and len(game[phase]) == 1:
                        playersProcessed += 1
                        game[phase] = None

                    if game[phase] is not None and len(game[phase]) > 1:
                        game[phase] = game[phase][1:]

    MIN_COUNT = 5
    output_filename2 = f'{fullPath}output2.arff'
    output_filename3 = f'{fullPath}output3.arff'
    output_filename4 = f'{fullPath}output4.arff'
    output_filename5 = f'{fullPath}output5.arff'
    output_filename6 = f'{fullPath}output6.arff'
    output_filename7 = f'{fullPath}output7.arff'
    output_filename8 = f'{fullPath}output8.arff'
    output_filename9 = f'{fullPath}output9.arff'
    output_filename10 = f'{fullPath}output10.arff'
    with open(output_filename2, "w") as fp2, open(output_filename3, "w") as fp3, open(output_filename4, "w") as fp4, open(
            output_filename5, "w") as fp5, open(output_filename6, "w") as fp6, open(output_filename7, "w") as fp7, open(
            output_filename8, "w") as fp8, open(output_filename9, "w") as fp9, open(output_filename10, "w") as fp10:
        files = {2: fp2, 3: fp3, 4: fp4, 5: fp5, 6: fp6, 7: fp7, 8: fp8, 9: fp9, 10: fp10}
        for playerCount, file in files.items():
            file.write('''@RELATION poker
    
@ATTRIBUTE pre_regular_call_nobody_before numeric
@ATTRIBUTE pre_regular_call_called_before numeric
@ATTRIBUTE pre_regular_call_raised_before numeric
@ATTRIBUTE pre_regular_raise_nobody_before numeric
@ATTRIBUTE pre_regular_raise_called_before numeric
@ATTRIBUTE pre_regular_raise_raised_before numeric
@ATTRIBUTE pre_button_call_nobody_before numeric
@ATTRIBUTE pre_button_call_called_before numeric
@ATTRIBUTE pre_button_call_raised_before numeric
@ATTRIBUTE pre_button_raise_nobody_before numeric
@ATTRIBUTE pre_button_raise_called_before numeric
@ATTRIBUTE pre_button_raise_raised_before numeric
@ATTRIBUTE post_regular_call_nobody_before numeric
@ATTRIBUTE post_regular_call_called_before numeric
@ATTRIBUTE post_regular_call_raised_before numeric
@ATTRIBUTE post_regular_raise_nobody_before numeric
@ATTRIBUTE post_regular_raise_called_before numeric
@ATTRIBUTE post_regular_raise_raised_before numeric
@ATTRIBUTE post_button_call_nobody_before numeric
@ATTRIBUTE post_button_call_called_before numeric
@ATTRIBUTE post_button_call_raised_before numeric
@ATTRIBUTE post_button_raise_nobody_before numeric
@ATTRIBUTE post_button_raise_called_before numeric
@ATTRIBUTE post_button_raise_raised_before numeric

@DATA
''')
        for name, data in finalData.items():
            for totalPlayers, stateData in data.items():
                inputData = []
                hasData = False
                for state, positionData in stateData.items():
                    for position, actionData in positionData.items():
                        for actionTaken, countData in actionData.items():
                            if countData['nobodyCount'] == 0 or countData['nobodyCount'] < MIN_COUNT:
                                inputData.append('?')
                            else:
                                hasData = True
                                value = countData['nobody'] / countData['nobodyCount']
                                inputData.append(str(value))
                            if countData['calledCount'] == 0 or countData['calledCount'] < MIN_COUNT:
                                inputData.append('?')
                            else:
                                hasData = True
                                value = countData['called'] / countData['calledCount']
                                inputData.append(str(value))
                            if countData['raisedCount'] == 0 or countData['raisedCount'] < MIN_COUNT:
                                inputData.append('?')
                            else:
                                hasData = True
                                value = countData['raised'] / countData['raisedCount']
                                inputData.append(str(value))
                if hasData:
                    fileToWriteTo = files[totalPlayers]
                    separator = ','
                    fileToWriteTo.write(separator.join(inputData))
                    fileToWriteTo.write("\n")
    print("Processed: {}".format(fullPath))
