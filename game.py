from pypokerengine.api.game import setup_config, start_poker
import time
from players.table4.Cluster0 import Cluster0
from players.table4.Cluster1 import Cluster1
from players.table4.Cluster2 import Cluster2
import weka.core.jvm as jvm


# starting JVM
jvm.start()
config = setup_config(max_round=100, initial_stack=10000, small_blind_amount=5)
c0 = Cluster0()
c0.name = 'c0.txt'
c1 = Cluster1()
c1.name = 'c1.txt'
c2 = Cluster2()
c2.name = 'c2.txt'
c02 = Cluster0()
c02.name = 'c02.txt'
config.register_player(name="cluster0", algorithm=c0)
config.register_player(name="cluster1", algorithm=c1)
config.register_player(name="cluster2", algorithm=c2)
config.register_player(name="cluster02", algorithm=c02)

start_time = time.time()
game_result = start_poker(config, verbose=0)

print(game_result)
print("--- %s seconds ---" % (time.time() - start_time))

# stopping JVM
jvm.stop()
