import pandas as pd
import matplotlib.pyplot as plt
from collections import defaultdict
import json
from math import ceil


def take_spread(sequence, num):
    length = float(len(sequence))
    return_value = []
    for var in range(num):
        index = int(ceil(var * length / num))
        return_value.append(sequence[index])

    return return_value


def nested_dict(n, type):
    if n == 1:
        return defaultdict(type)
    else:
        return defaultdict(lambda: nested_dict(n-1, type))


finalData = nested_dict(3, int)
# with open(f"H:\\Universitate\\meta_data\\results\\10players\\c0.txt", 'r') as f0, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c1.txt", 'r') as f1, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c2.txt", 'r') as f2, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c3.txt", 'r') as f3, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c32.txt", 'r') as f32, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c33.txt", 'r') as f33, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c4.txt", 'r') as f4, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c5.txt", 'r') as f5, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c6.txt", 'r') as f6, open(
#         f"H:\\Universitate\\meta_data\\results\\10players\\c7.txt", 'r') as f7:
#     files = {0: [f0], 1: [f1], 2: [f2], 3: [f3, f32, f33], 4: [f4], 5: [f5], 6: [f6], 7: [f7]}
with open(f"H:\\Universitate\\meta_data\\results\\8players\\c0.txt", 'r') as f0, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c1.txt", 'r') as f1, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c2.txt", 'r') as f2, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c3.txt", 'r') as f3, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c32.txt", 'r') as f32, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c12.txt", 'r') as f12, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c4.txt", 'r') as f4, open(
        f"H:\\Universitate\\meta_data\\results\\8players\\c42.txt", 'r') as f42:
    files = {0: [f0], 1: [f1, f12], 2: [f2], 3: [f3, f32], 4: [f4, f42]}
# with open(f"H:\\Universitate\\meta_data\\results\\4players\\c0.txt", 'r') as f0, open(
#         f"H:\\Universitate\\meta_data\\results\\4players\\c1.txt", 'r') as f1, open(
#         f"H:\\Universitate\\meta_data\\results\\4players\\c2.txt", 'r') as f2, open(
#         f"H:\\Universitate\\meta_data\\results\\4players\\c02.txt", 'r') as f02:
#     files = {0: [f0, f02], 1: [f1], 2: [f2]}
    for correct_id, files in files.items():
        for file in files:
            data = []
            for line in file:
                if line != "\n":
                    data.append(json.loads(line))
            for x, types in enumerate(data):
                if x == 1:
                    types = take_spread(types, 100)
                for round_number, values in enumerate(types):
                    min_value = 10
                    min_value_id = None
                    max_value = 0
                    max_value_id = None
                    for i, value in enumerate(values):
                        if value > max_value:
                            max_value = value
                            max_value_id = i
                        if value < min_value:
                            min_value = value
                            min_value_id = i
                    correct = False
                    if x == 0:
                        if min_value_id == correct_id:
                            correct = True
                    else:
                        if max_value_id == correct_id:
                            correct = True
                    round_number = str(round_number)
                    finalData[x][round_number]['total'] += 1
                    if correct:
                        finalData[x][round_number]['correct'] += 1

print(finalData)
preparedData = {}
for x, type_data in finalData.items():
    x = str(x)
    preparedData['x_values'] = range(100)
    preparedData[x] = []
    for i, round_data in type_data.items():
        value = round_data['correct'] / round_data['total']
        preparedData[x].append(value)

print(preparedData)
df = pd.DataFrame(preparedData)
# multiple line plots
plt.plot('x_values', '0', label='Eiklīda attālums', data=df, linewidth=2, alpha=0.7)
plt.plot('x_values', '1', label='Beijesa modelis', data=df, linewidth=2, alpha=0.7)
plt.plot('x_values', '2', label='AdaBoost ar LMT klasifikatoru', data=df, linewidth=2, alpha=0.7)
plt.plot('x_values', '3', label='Vairākslāņu perceptrons', data=df, linewidth=2, alpha=0.7)
plt.plot('x_values', '4', label='Beijesa tīkls', data=df, linewidth=2, alpha=0.7)
# show legend
plt.legend()

# show graph
plt.show()
