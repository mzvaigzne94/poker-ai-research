from pypokerengine.utils.card_utils import gen_cards, estimate_hole_card_win_rate


def simulate_hands():
    player_count = 9
    ranks = {'A': 14, 'K': 13, 'Q': 12, 'J': 11, 'T': 10, '9': 9, '8': 8, '7': 7, '6': 6, '5': 5, '4': 4, '3': 3,
             '2': 2}
    suit_one = 'C'
    suit_two = 'D'
    hand_ranks = []

    for rankOne in ranks.keys():
        for rankTwo in ranks.keys():
            if ranks[rankTwo] < ranks[rankOne]:
                suited = True
                hole_card = gen_cards([f'{suit_one}{rankOne}', f'{suit_one}{rankTwo}'])
            else:
                suited = False
                hole_card = gen_cards([f'{suit_one}{rankOne}', f'{suit_two}{rankTwo}'])

            win_rate = estimate_hole_card_win_rate(nb_simulation=20000, nb_player=player_count, hole_card=hole_card,
                                                   community_card=None)
            print(f'{win_rate} {rankOne} {rankTwo} {suited}')
            hand_ranks.append({'winRate': win_rate, 'rankOne': rankOne, 'rankTwo': rankTwo, 'isSuited': suited})

    hand_ranks = sorted(hand_ranks, key=lambda k: k['winRate'], reverse=True)
    print(hand_ranks)
